<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <?php include "cat_menu.php"; ?>  

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li>          
                    <li><a title="ABOUT US" class="active" href="about.php">ABOUT US</a></li>        
                </ol>
            </div>
        </section>

        <?php include "search_box.php"; ?>

        <section class="row-fluid"> 
            <div class="container">                
                <h1 class="heading-title row-fluid">ABOUT US</h1>
            </div>
        </section>

        <section class="row-fluid panel-padding">
            <div class="container">
                <div class="logo-center">
                    <img alt="" class="" src="images/logo-01.png?v=1" />                    
                </div>
                <center>
                    <h2 class="heading-2">BIG HOME FURNISH</h2>
                    <article class="plain-text texe-black form-group">
                        <p>
                        ທ່ານບໍ່ຕ້ອງການທີ່ຈະເຂົ້າໄປເບິ່ງໃນເວັບໄຊທ໌ຂອງທ່ານ, ແລະທ່ານຈະບໍ່ຕ້ອງການທີ່ຈະປະຕິບັດຫນ້ານີ້. 
                        Pro ex appareat appareamus, ປະກອບສ່ວນສໍາລັບການປະຕິບັດງານຂອງຂ້າພະເຈົ້າ, 
                        ຂ້າພະເຈົ້າໄດ້ຮັບການສະຫນັບສະຫນູນ. Clita ignota ລວມທັງຫມົດແມ່ນຜູ້ທີ່ຕ້ອງການທີ່ຈະໄດ້ຮັບຄວາມນິຍົມ. 
                        ຖ້າທ່ານຕ້ອງການທີ່ຈະສົ່ງຂໍ້ຄວາມ, ທ່ານຈະຕ້ອງໃຊ້ເວບໄຊທ໌ໃດຫນຶ່ງ. Aperiam diceret mellore in has, 
                        cu graeci pericula has ທ່ານບໍ່ຕ້ອງການທີ່ຈະເຂົ້າໄປເບິ່ງໃນເວັບໄຊທ໌ຂອງທ່ານ, ແລະທ່ານຈະບໍ່ຕ້ອງການທີ່ຈະປະຕິບັດຫນ້ານີ້. 
                        Pro ex appareat appareamus, ປະກອບສ່ວນສໍາລັບການປະຕິບັດງານຂອງຂ້າພະເຈົ້າ, ຂ້າພະເຈົ້າໄດ້ຮັບການສະຫນັບສະຫນູນ. 
                        Clita ignota ລວມທັງຫມົດແມ່ນຜູ້ທີ່ຕ້ອງການທີ່ຈະໄດ້ຮັບຄວາມນິຍົມ. ຖ້າທ່ານຕ້ອງການທີ່ຈະສົ່ງຂໍ້ຄວາມ, ທ່ານຈະຕ້ອງໃຊ້ເວບໄຊທ໌ໃດຫນຶ່ງ. 
                        Aperiam diceret mellore in has, cu graeci pericula has ທ່ານບໍ່ຕ້ອງການທີ່ຈະເຂົ້າໄປເບິ່ງໃນເວັບໄຊທ໌ຂອງທ່ານ, 
                        ແລະທ່ານຈະບໍ່ຕ້ອງການທີ່ຈະປະຕິບັດຫນ້ານີ້. Pro ex appareat appareamus, ປະກອບສ່ວນສໍາລັບການປະຕິບັດງານຂອງຂ້າພະເຈົ້າ, 
                        ຂ້າພະເຈົ້າໄດ້ຮັບການສະຫນັບສະຫນູນ. Clita ignota ລວມທັງຫມົດແມ່ນຜູ້ທີ່ຕ້ອງການທີ່ຈະໄດ້ຮັບຄວາມນິຍົມ. 
                        ຖ້າທ່ານຕ້ອງການທີ່ຈະສົ່ງຂໍ້ຄວາມ, ທ່ານຈະຕ້ອງໃຊ້ເວບໄຊທ໌ໃດຫນຶ່ງ. Aperiam diceret mellore in has, 
                        cu graeci pericula has vix ea ridens tincidunt Clita ignota 
                        ລວມທັງຫມົດແມ່ນຜູ້ທີ່ຕ້ອງການທີ່ຈະໄດ້ຮັບຄວາມນິຍົມ. ຖ້າທ່ານຕ້ອງການທີ່ຈະສົ່ງຂໍ້ຄວາມ, ທ່ານຈະຕ້ອງໃຊ້ເວບໄຊທ໌ໃດຫນຶ່ງ.
                            Lorem ipsum dolor sit amet, ei nonumy inciderint vim, vel dicant nusquam te. 
                            Pro ex appareat accusamus, constituto percipitur cu mei, vix ea ridens tincidunt. 
                            Clita ignota concludaturque ad est, qui ex dicam vulputate.
                            Qui an quas postea quaestio, te timeam maluisset constituam usu. 
                            Aperiam diceret meliore in has, cu graeci pericula has.
                            vix ea ridens tincidunt. 
                            Clita ignota concludaturque ad est, qui ex dicam vulputate.
                            Qui an quas postea quaestio, te timeam maluisset constituam usu. 
                        </p>
                    <article>
                </center>
                <div class="row">
                    <div class="col-md-4 full-width-xs form-group"> 
                        <div class="people-img">
                            <img alt="" class="cycle" src="images/people-image-1.jpg?v=1" />
                        </div>
                        <h3 class="plain-text text-center">DONALD HANKEN</h3>
                    </div>
                    <div class="col-md-4 full-width-xs form-group">
                        <div class="people-img">
                            <img alt="" class="cycle" src="images/people-image-2.jpg?v=1" />
                        </div>
                        <h3 class="plain-text text-center">JANETTE TELFER</h3>
                    </div>
                    <div class="col-md-4 full-width-xs form-group">
                        <div class="people-img">
                            <img alt="" class="cycle" src="images/people-image-3.jpg?v=1" />
                        </div>
                        <h3 class="plain-text text-center">LEANNE WOLF</h3>
                    </div>
                </div>            
            </div>
        </section>

        <section class="row-fluid panel-padding bg-softgray bg-img">
            <div class="container">
                <center><h2 class="heading-2">ORIGINS<h2></center>
                <div class="row">
                    <div class="col-md-6 full-width-xs form-group">
                        <article class="plain-text details-border texe-black ">
                            <p>
                                Lorem ipsum dolor sit amet, ei nonumy inciderint vim, vel dicant nusquam te. 
                                Pro ex appareat accusamus, constituto percipitur cu mei, vix ea ridens tincidunt. 
                                Clita ignota concludaturque ad est, qui ex dicam vulputate.
                                Qui an quas postea quaestio, te timeam maluisset constituam usu. 
                                Aperiam diceret meliore in has, cu graeci pericula has.
                                Lorem ipsum dolor sit amet, ei nonumy inciderint vim, vel dicant nusquam te. 
                                Pro ex appareat accusamus, constituto percipitur cu mei, vix ea ridens tincidunt. 
                                Clita ignota concludaturque ad est, qui ex dicam vulputate.
                                Qui an quas postea quaestio, te timeam maluisset constituam usu. 
                                Aperiam diceret meliore in has, cu graeci pericula has.
                                Lorem ipsum dolor sit amet, ei nonumy inciderint vim, vel dicant nusquam te. 
                                Pro ex appareat accusamus, constituto percipitur cu mei, vix ea ridens tincidunt. 
                                Clita ignota concludaturque ad est, qui ex dicam vulputate.
                                Qui an quas postea quaestio, te timeam maluisset constituam usu. 
                                Aperiam diceret meliore in has, cu graeci pericula has.
                                vix ea ridens tincidunt. 
                                Clita ignota concludaturque ad est, qui ex dicam vulputate.
                                Qui an quas postea quaestio, te timeam maluisset constituam usu.    
                            </p>                         
                        <article>
                    </div>
                    <div class="col-md-6 full-width-xs form-group">
                        <div class="row">
                            <div class="col-md-6 full-width-xs form-group">
                                <div class="card-item with-bottom">
                                    <img class="full-width form-group" src="http://bridge221.qodeinteractive.com/wp-content/uploads/2018/04/slider-1.jpg">
                                    <article class="plain-text texe-black">
                                        diceret meliore in has, <br>19877
                                    </article>
                                </div>
                            </div>
                            <div class="col-md-6 full-width-xs form-group">
                                <div class="card-item with-bottom">
                                    <img class="full-width form-group" src="images/product-2.png?v=1">
                                    <article class="plain-text texe-black">
                                        diceret meliore in has, <br>19877
                                    </article>
                                </div>                                
                            </div>
                            <div class="col-md-6 full-width-xs form-group">
                                <div class="card-item with-bottom">
                                    <img class="full-width form-group" src="images/product-3.png?v=1">
                                    <article class="plain-text texe-black">
                                        diceret meliore in has, <br>19877
                                    </article>
                                </div>
                            </div>
                            <div class="col-md-6 full-width-xs form-group">
                                <div class="card-item with-bottom">
                                    <img class="full-width form-group" src="images/product-2.png?v=1">
                                    <article class="plain-text texe-black">
                                        diceret meliore in has, <br>19877
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="row-fluid panel-padding">
            <div class="container">
                <center><h2 class="heading-2">CREATE<h2></center>
                <div class="row">
                    <div class="col-md-6 full-width-xs form-group">
                        <div class="card-item">
                            <div class="images">
                                <img class="full-width form-group" src="http://bridge221.qodeinteractive.com/wp-content/uploads/2018/04/slider-1.jpg">
                            </div>
                            <article class="plain-text texe-black">
                                <p>
                                    Lorem ipsum dolor sit amet, ei nonumy inciderint vim, vel dicant nusquam te. 
                                    Pro ex appareat accusamus, constituto percipitur cu mei, vix ea ridens tincidunt. 
                                    Clita ignota concludaturque ad est, qui ex dicam vulputate.
                                    Qui an quas postea quaestio, te timeam maluisset constituam usu. 
                                    Aperiam diceret meliore in has, cu graeci pericula has.
                                </p>
                            <article>
                        </div>
                    </div>
                    <div class="col-md-6 full-width-xs form-group">
                        <div class="card-item">
                            <div class="images">
                                <img class="full-width form-group" src="images/about-image-1.jpg?v=1">
                            </div>                            
                            <article class="plain-text details texe-black">
                                <p>
                                    Lorem ipsum dolor sit amet, ei nonumy inciderint vim, vel dicant nusquam te. 
                                    Pro ex appareat accusamus, constituto percipitur cu mei, vix ea ridens tincidunt. 
                                    Clita ignota concludaturque ad est, qui ex dicam vulputate.
                                    Qui an quas postea quaestio, te timeam maluisset constituam usu. 
                                    Aperiam diceret meliore in has, cu graeci pericula has.
                                </p>
                            <article>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">
                <h2 class="heading-title row-fluid">ABOUT US</h2>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>

    </div>    

    <?php include "footer.php"; ?>

</body>
</html>