<div class="col-lg-3 col-sm-3 form-group account-aside">

    <div class="account-aside-toggle">
        <a id="aside-toggle">
            MENU<i class="material-icons"></i>
        </a> 
    </div>
    <div class="account-aside-menu">
        <ul>
            <li class="active">
                <a href="account_dashboard.php">
                    <i class="material-icons">settings</i>
                    ACCOUNT DASHBOARD
                </a>
            </li>
            <li>
                <a href="account_info.php">
                    <i class="material-icons">person</i>
                    ACCOUNT INFORMATION
                </a>
            </li>
            <li>
                <a href="account_addressbook.php">
                    <i class="material-icons">pin_drop</i>                        
                    ADDRESS BOOK
                </a>
            </li>
            <li>
                <a href="account_order.php">
                    <i class="material-icons">shopping_basket</i>
                    MY ORDER
                </a>
            </li>
        </ul>
    </div>
</div>