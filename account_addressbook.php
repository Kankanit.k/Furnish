<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li>          
                    <li><a title="ADDRESS BOOK" class="active" href="account_addressbook.php">ADDRESS BOOK</a></li>        
                </ol>
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">                
                <h1 class="heading-title row-fluid">ADDRESS BOOK</h1>
            </div>
        </section>

         <section class="row-fluid panel-padding-half">
            <div class="container">
                <div class="row-fluid form-group">
                    <div class="row">

                        <?php include "accound_aside.php";?>

                        <div class="col-lg-9 col-sm-9 form-group account-wrapper">
                            <div class="account-content">

                                <div class="element-wrap">
                                    <div class="row">
                                        <div class="col-md-6 full-width-xs">
                                            <h2 class="row-fluid content-title">ADD SHIPPPING ADDRESS</h2>                                    
                                            <div class="row-fluid panel-element">
                                                <form role="form" method="" class="form-sign-up none-bg">                                                
                                                    <input type="text" name="" class="input-control form-group" placeholder="*FIRST NAME">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*LAST NAME">
                                                    <input type="text" name="" class="input-control form-group" placeholder="COMPANY">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*MOBILE">
                                                    <input type="text" name="" class="input-control form-group" placeholder="FAX">
                                                    <input type="text" name="" class="input-control form-group" placeholder="TELEPHONE">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*STREET ADDRESS">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*STATE PROVINCE">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*CITY">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*SUB DISTRICT">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*ZIPCODE">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*COUNTRY">
                                                    <button type="submit" class="btn btn-send-email form-group">SAVE</button>
                                                </form>                                        
                                            </div>
                                            <div class="row-fluid panel-element select-shipping">                                                 
                                                <label class="control control--radio">
                                                    <input type="radio" name="radio1" checked=""/>
                                                    <div class="control__indicator"></div>
                                                </label>                        
                                                <div class="row-fluid form-group">  
                                                    <h2 class="row-fluid content-title">SHIPPPING ADDRESS</h2> 
                                                    <p>
                                                        MISS KANKANIT KONGTHONG<br>
                                                        120/34-35 MOO 24<br>
                                                        SILA SUB DISTRICT, <br>
                                                        MUEANG KHONKAEN DISTRICT,<br> 
                                                        KHONKAEN PROVINCE. 40000, <br>
                                                        THAILAND
                                                    </p>
                                                </div>
                                                <button type="submit" class="btn btn-send-email btn-edit" style="margin-bottom:15px" data-target="#btnEditShipping"  data-toggle="modal">
                                                    EDIT
                                                </button>
                                            </div>
                                            <div class="row-fluid panel-element select-shipping"> 
                                                <label class="control control--radio">
                                                    <input type="radio" name="radio1" />
                                                    <div class="control__indicator"></div>
                                                </label>                           
                                                <div class="row-fluid form-group">  
                                                    <h2 class="row-fluid content-title">SHIPPPING ADDRESS</h2>  
                                                    <p>
                                                        MISS KANKANIT KONGTHONG<br>
                                                        120/34-35 MOO 24<br>
                                                        SILA SUB DISTRICT, <br>
                                                        MUEANG KHONKAEN DISTRICT,<br> 
                                                        KHONKAEN PROVINCE. 40000, <br>
                                                        THAILAND
                                                    </p>
                                                </div>
                                                <button type="submit" class="btn btn-send-email btn-edit" style="margin-bottom:15px" data-target="#btnEditShipping"  data-toggle="modal">
                                                    EDIT
                                                </button>
                                            </div>  
                                            <div class="row-fluid panel-element select-shipping">  
                                                <label class="control control--radio">
                                                    <input type="radio" name="radio1" />
                                                    <div class="control__indicator"></div>
                                                </label>                          
                                                <div class="row-fluid form-group">  
                                                    <h2 class="row-fluid content-title">SHIPPPING ADDRESS</h2>  
                                                    <p>
                                                        MISS KANKANIT KONGTHONG<br>
                                                        120/34-35 MOO 24<br>
                                                        SILA SUB DISTRICT, <br>
                                                        MUEANG KHONKAEN DISTRICT,<br> 
                                                        KHONKAEN PROVINCE. 40000, <br>
                                                        THAILAND
                                                    </p>
                                                </div>
                                                <button type="submit" class="btn btn-send-email btn-edit" style="margin-bottom:15px" data-target="#btnEditShipping"  data-toggle="modal">
                                                    EDIT
                                                </button>
                                            </div>  
                                            <div class="row-fluid panel-element select-shipping">   
                                                <label class="control control--radio">
                                                    <input type="radio" name="radio1" />
                                                    <div class="control__indicator"></div>
                                                </label>                              
                                                <div class="row-fluid form-group"> 
                                                    <h2 class="row-fluid content-title">SHIPPPING ADDRESS</h2>        
                                                    <p>
                                                        MISS KANKANIT KONGTHONG<br>
                                                        120/34-35 MOO 24<br>
                                                        SILA SUB DISTRICT, <br>
                                                        MUEANG KHONKAEN DISTRICT,<br> 
                                                        KHONKAEN PROVINCE. 40000, <br>
                                                        THAILAND
                                                    </p>
                                                </div>
                                                <button type="submit" class="btn btn-send-email btn-edit" style="margin-bottom:15px" data-target="#btnEditShipping"  data-toggle="modal">
                                                    EDIT
                                                </button>
                                            </div> 
                                        </div>
                                        <div class="col-md-6 full-width-xs">
                                            <h2 class="row-fluid content-title">ADD BILLING ADDRESS</h2>                                    
                                            <div class="row-fluid panel-element">
                                                <form role="form" method="" class="form-sign-up none-bg">                                                
                                                    <input type="text" name="" class="input-control form-group" placeholder="*FIRST NAME">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*LAST NAME">
                                                    <input type="text" name="" class="input-control form-group" placeholder="COMPANY">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*MOBILE">
                                                    <input type="text" name="" class="input-control form-group" placeholder="FAX">
                                                    <input type="text" name="" class="input-control form-group" placeholder="TELEPHONE">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*STREET ADDRESS">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*STATE PROVINCE">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*CITY">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*SUB DISTRICT">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*ZIPCODE">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*COUNTRY">
                                                    <button type="submit" class="btn btn-send-email form-group">SAVE</button>
                                                </form>                                        
                                            </div>  

                                            <div class="row-fluid panel-element select-billing">  
                                                <label class="control control--radio">
                                                    <input type="radio" name="radio" checked="checked"/>
                                                    <div class="control__indicator"></div>
                                                </label>           
                                                <div class="row-fluid form-group">  
                                                    <h2 class="row-fluid content-title">BILLING ADDRESS</h2> 
                                                    <p>
                                                        MISS KANKANIT KONGTHONG<br>
                                                        120/34-35 MOO 24<br>
                                                        SILA SUB DISTRICT, <br>
                                                        MUEANG KHONKAEN DISTRICT,<br> 
                                                        KHONKAEN PROVINCE. 40000, <br>
                                                        THAILAND
                                                    </p>
                                                </div>
                                                <button type="submit" class="btn btn-send-email btn-edit" style="margin-bottom:15px" data-target="#btnEditBilling"  data-toggle="modal">
                                                    EDIT
                                                </button>
                                            </div>
                                            <div class="row-fluid panel-element select-billing">
                                                <label class="control control--radio">
                                                    <input type="radio" name="radio" />
                                                    <div class="control__indicator"></div>
                                                </label>              
                                                <div class="row-fluid form-group">  
                                                    <h2 class="row-fluid content-title">BILLING ADDRESS</h2>  
                                                    <p>
                                                        MISS KANKANIT KONGTHONG<br>
                                                        120/34-35 MOO 24<br>
                                                        SILA SUB DISTRICT, <br>
                                                        MUEANG KHONKAEN DISTRICT,<br> 
                                                        KHONKAEN PROVINCE. 40000, <br>
                                                        THAILAND
                                                    </p>
                                                </div>
                                                <button type="submit" class="btn btn-send-email btn-edit" style="margin-bottom:15px" data-target="#btnEditBilling"  data-toggle="modal">
                                                    EDIT
                                                </button>
                                            </div>  
                                            <div class="row-fluid panel-element select-billing">  
                                                <label class="control control--radio">
                                                    <input type="radio" name="radio" />
                                                    <div class="control__indicator"></div>
                                                </label>                       
                                                <div class="row-fluid form-group">  
                                                    <h2 class="row-fluid content-title">BILLING ADDRESS</h2>  
                                                    <p>
                                                        MISS KANKANIT KONGTHONG<br>
                                                        120/34-35 MOO 24<br>
                                                        SILA SUB DISTRICT, <br>
                                                        MUEANG KHONKAEN DISTRICT,<br> 
                                                        KHONKAEN PROVINCE. 40000, <br>
                                                        THAILAND
                                                    </p>
                                                </div>
                                                <button type="submit" class="btn btn-send-email btn-edit" style="margin-bottom:15px" data-target="#btnEditBilling"  data-toggle="modal">
                                                    EDIT
                                                </button>
                                            </div>  
                                            <div class="row-fluid panel-element select-billing">   
                                                <label class="control control--radio">
                                                    <input type="radio" name="radio" />
                                                    <div class="control__indicator"></div>
                                                </label>                               
                                                <div class="row-fluid form-group"> 
                                                    <h2 class="row-fluid content-title">BILLING ADDRESS</h2>        
                                                    <p>
                                                        MISS KANKANIT KONGTHONG<br>
                                                        120/34-35 MOO 24<br>
                                                        SILA SUB DISTRICT, <br>
                                                        MUEANG KHONKAEN DISTRICT,<br> 
                                                        KHONKAEN PROVINCE. 40000, <br>
                                                        THAILAND
                                                    </p>
                                                </div>
                                                <button type="submit" class="btn btn-send-email btn-edit" style="margin-bottom:15px" data-target="#btnEditBilling"  data-toggle="modal">
                                                    EDIT
                                                </button>
                                            </div>                                          
                                        </div>
                                    </div>
                                </div>        
                               
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">
                <h2 class="heading-title row-fluid">ADDRESS BOOK</h2>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>

    </div>   

    <!-- Modal Edit Billing -->
    <div class="modal fade bd-example-modal-lg" id="btnEditShipping" tabindex="-1" role="dialog" aria-labelledby="btnEditShipping" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="" style="float:left; line-height: 35px;">EDIT SHIPPING ADDRESS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-lg-6 form-group">
                            <label for="">*FIRST NAME</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*FIRST NAME">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*LAST NAME</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*LAST NAME">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">COMPANY</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="COMPANY">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*MOBILE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*MOBILE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">FAX</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="FAX">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">TELEPHONE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="TELEPHONE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*STREET ADDRESS</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*STREET ADDRESS">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*STATE PROVINCE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*STATE PROVINCE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*CITY</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*CITY">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*SUB DISTRICT</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*SUB DISTRICT">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*ZIPCODE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*ZIPCODE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*COUNTRY</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*COUNTRY">
                        </div>
                    </div>                   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary">SAVE</button>
            </div>
            </div>
        </div>
    </div> 

    <!-- Modal Edit Billing -->
    <div class="modal fade bd-example-modal-lg" id="btnEditBilling" tabindex="-1" role="dialog" aria-labelledby="btnEditBilling" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="" style="float:left; line-height: 35px;">EDIT BILLING ADDRESS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-lg-6 form-group">
                            <label for="">*FIRST NAME</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*FIRST NAME">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*LAST NAME</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*LAST NAME">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">COMPANY</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="COMPANY">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*MOBILE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*MOBILE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">FAX</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="FAX">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">TELEPHONE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="TELEPHONE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*STREET ADDRESS</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*STREET ADDRESS">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*STATE PROVINCE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*STATE PROVINCE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*CITY</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*CITY">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*SUB DISTRICT</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*SUB DISTRICT">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*ZIPCODE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*ZIPCODE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*COUNTRY</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*COUNTRY">
                        </div>
                    </div>                   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary">SAVE</button>
            </div>
            </div>
        </div>
    </div> 

    <?php include "footer.php"; ?>

</body>
</html>