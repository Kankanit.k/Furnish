<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li>          
                    <li><a title="ACCOUNT DASHBOARD" class="active" href="account_dashboard.php">ACCOUNT DASHBOARD</a></li>        
                </ol>
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">                
                <h1 class="heading-title row-fluid">ACCOUNT DASHBOARD</h1>
            </div>
        </section>

        <section class="row-fluid panel-padding-half">
            <div class="container">
                <div class="row-fluid form-group">
                    <div class="row">

                        <?php include "accound_aside.php";?>

                        <div class="col-lg-9 col-sm-9 form-group account-wrapper">
                            <div class="account-content">

                                <div class="element-wrap">
                                    <a href="account_info.php" class="btn btn-rigth text-yellow" >EDIT</a>
                                    <h2 class="row-fluid content-title">CONTACT INFORMATION</h2>                                    
                                    <div class="row-fluid panel-element">

                                        <span class="row-fluid form-group text plain-text">
                                            MISS KANKANIT KONGTHONG
                                        </span>
                                        <span class="row-fluid form-group text plain-text">
                                            KANKANIT.3089@GMAIL.COM
                                        </span>
                                    </div>
                                </div>
                                
                                <div class="element-wrap">
                                    <a href="account_addressbook.php" class="btn btn-rigth text-yellow">EDIT</a>
                                    <h2 class="content-title">SHIPPING ADDRESS AND BILLING ADDRESS</h2>
                                    <div class="row-fluid panel-element">
                                        <div class="row-fluid form-group">                                            
                                            <h5 class="row-fluid "><strong>DEFAULT SHIPPING ADDRESS</strong></h5>
                                            <p>
                                                MISS KANKANIT KONGTHONG<br>
                                                120/34-35 MOO 24<br>
                                                SILA SUB DISTRICT, <br>
                                                MUEANG KHONKAEN DISTRICT,<br> 
                                                KHONKAEN PROVINCE. 40000, <br>
                                                THAILAND
                                            </p>
                                        </div>
                                        <div class="row-fluid form-group">                                            
                                            <h5 class="row-fluid "><strong>DEFAULT BILLING ADDRESS</strong></h5>
                                            <p>
                                                MISS KANKANIT KONGTHONG<br>
                                                120/34-35 MOO 24<br>
                                                SILA SUB DISTRICT, <br>
                                                MUEANG KHONKAEN DISTRICT,<br> 
                                                KHONKAEN PROVINCE. 40000, <br>
                                                THAILAND
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row-fluid form-group">
                                    <label class="control control--checkbox">NEWSLETTER E-MAIL
                                        <input type="checkbox"/>
                                        <div class="control__indicator"></div>
                                    </label>
                                    <div class="row-fluid empty-space" style="margin-top:50px;"></div>
                                </div>

                                <div class="element-wrap">
                                    <a href="account_order.php" class="btn btn-rigth text-yellow">VIEW ALL</a>
                                    <h2 class="content-title">RECENT ORDERS</h2>
                                    <div class="row-fluid">

                                         <div class="table-responsive table-custom">    
                                            <div class="tb-outer">
                                                <div class="tb-header">
                                                    <ul>
                                                        <li class="pd-id hide-xs">ID</li>
                                                        <li>DATE</li>
                                                        <li>ORDER#</li>
                                                        <li class="pd-code hide-xs">PRODUCT CODE</li>
                                                        <li>PRICE</li>
                                                        <li class="hide-xs">SHIP TO</li>
                                                        <li>STATUS</li>
                                                    </ul>
                                                </div>
                                                <div class="tb-body">
                                                    <a href="pending.php" class="order-list">
                                                        <ul>
                                                            <li class="pd-id hide-xs">1</li>
                                                            <li>09-06-2561</li>
                                                            <li>000001</li>
                                                            <li class="pd-code hide-xs">KU895u1</li>
                                                            <li>250.00</li>
                                                            <li class="hide-xs">BIG HOME FURNISH LAOS COUNTRY</li>
                                                            <li>PENDING</li>                                                        
                                                        </ul>
                                                    </a>
                                                    <?php for ($i=2; $i<=4 ; $i++) {?>
                                                        <a href="account_orderdetail.php" class="order-list">
                                                            <ul>
                                                                <li class="pd-id hide-xs"><?php echo "$i"; ?></li>
                                                                <li>09-06-2561</li>
                                                                <li>00000<?php echo "$i"; ?></li>
                                                                <li class="pd-code hide-xs">KU895u<?php echo "$i"; ?></li>
                                                                <li>250.00</li>
                                                                <li class="hide-xs">BIG HOME FURNISH LAOS COUNTRY</li>
                                                                <li>TRANSPORT</li>                                                        
                                                            </ul>
                                                        </a>
                                                    <?php } ?>
                                                    <a href="account_orderdetail.php" class="order-list">
                                                        <ul>
                                                            <li class="pd-id hide-xs">5</li>
                                                            <li>09-06-2561</li>
                                                            <li>000005</li>
                                                            <li class="pd-code hide-xs">KU895u5</li>
                                                            <li>250.00</li>
                                                            <li class="hide-xs">BIG HOME FURNISH LAOS COUNTRY</li>
                                                            <li>RECEIVED</li>                                                        
                                                        </ul>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">
                <h2 class="heading-title row-fluid">ACCOUNT DASHBOARD</h2>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>

    </div>    

    <!-- Modal edit info -->
    <div class="modal fade " id="btnEditInfo" tabindex="-1" role="dialog" aria-labelledby="btnEditInfo" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="" style="float:left; line-height: 35px;">EDIT CONTACT INFORMATION</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" class="input-control" id="" aria-describedby="" placeholder="Enter Name">
                    </div>
                    <div class="form-group">
                        <label for="">Email address</label>
                        <input type="email" class="input-control" id="" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary">SAVE</button>
            </div>
            </div>
        </div>
    </div>

     <!-- Modal Edit Shipping -->
     <div class="modal fade bd-example-modal-lg" id="btnEditShipping" tabindex="-1" role="dialog" aria-labelledby="btnEditShipping" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="" style="float:left; line-height: 35px;">EDIT SHIPPING ADDRESS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-lg-6 form-group">
                            <label for="">*FIRST NAME</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*FIRST NAME">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*LAST NAME</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*LAST NAME">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">COMPANY</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="COMPANY">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*MOBILE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*MOBILE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">FAX</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="FAX">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">TELEPHONE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="TELEPHONE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*STREET ADDRESS</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*STREET ADDRESS">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*STATE  PROVINCE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*STATE  PROVINCE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*CITYS</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*CITY">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*SUB DISTRICT</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*SUB DISTRICT">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*ZIPCODE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*ZIPCODE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*COUNTRY</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*COUNTRY">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary">SAVE</button>
            </div>
            </div>
        </div>
    </div> 

    <!-- Modal Edit Billing -->
    <div class="modal fade bd-example-modal-lg" id="btnEditBilling" tabindex="-1" role="dialog" aria-labelledby="btnEditBilling" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="" style="float:left; line-height: 35px;">EDIT BILLING ADDRESS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-lg-6 form-group">
                            <label for="">*FIRST NAME</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*FIRST NAME">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*LAST NAME</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*LAST NAME">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">COMPANY</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="COMPANY">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*MOBILE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*MOBILE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">FAX</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="FAX">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">TELEPHONE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="TELEPHONE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*STREET ADDRESS</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*STREET ADDRESS">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*STATE PROVINCE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*STATE PROVINCE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*CITY</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*CITY">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*SUB DISTRICT</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*SUB DISTRICT">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*ZIPCODE</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*ZIPCODE">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">*COUNTRY</label>
                            <input type="text" class="input-control form-group" id="" aria-describedby="" placeholder="*COUNTRY">
                        </div>
                    </div>                   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary">SAVE</button>
            </div>
            </div>
        </div>
    </div>

   

    <?php include "footer.php"; ?>

</body>
</html>