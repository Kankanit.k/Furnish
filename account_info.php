<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li>          
                    <li><a title="ACCOUNT INFORMATION" class="active" href="account_info.php">ACCOUNT INFORMATION</a></li>        
                </ol>
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">                
                <h1 class="heading-title row-fluid">ACCOUNT INFORMATION</h1>
            </div>
        </section>

        <section class="row-fluid panel-padding-half">
            <div class="container">
                <div class="row-fluid form-group">
                    <div class="row">

                        <?php include "accound_aside.php";?>

                        <div class="col-lg-9 col-sm-9 form-group account-wrapper">
                            <div class="account-content">

                                <div class="element-wrap">
                                    <h2 class="row-fluid content-title">ACCOUNT INFORMATION</h2>                                    
                                    <div class="row-fluid panel-element">
                                        <form role="form" method="" class="form-sign-up none-bg">
                                            <div class="row">
                                                <div class="col-md-6 full-width-xs">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*FIRST NAME">
                                                    <input type="text" name="" class="input-control form-group" placeholder="*LAST NAME">
                                                    <input type="email" name="" class="input-control form-group" placeholder="*EMAIL">
                                                    <span class="row-fluid text plain-text form-group">BIRTH DATE</span>
                                                    <div class="row-fluid">
                                                        <div class="row">
                                                            <div class="col-lg-4 form-group">
                                                                <div class="b-day">
                                                                    <select class="form-control">
                                                                        <option>1</option>
                                                                        <option>2</option>
                                                                        <option>3</option>
                                                                        <option>5</option>
                                                                        <option>6</option>
                                                                        <option>7</option>
                                                                        <option>8</option>
                                                                        <option>9</option>
                                                                        <option>10</option>
                                                                        <option>11</option>
                                                                        <option>12</option>
                                                                        <option>13</option>
                                                                        <option>14</option>
                                                                        <option>15</option>
                                                                        <option>16</option>
                                                                        <option>17</option>
                                                                        <option>18</option>
                                                                        <option>19</option>
                                                                        <option>20</option>
                                                                        <option>21</option>
                                                                        <option>22</option>
                                                                        <option>23</option>
                                                                        <option>24</option>
                                                                        <option>25</option>
                                                                        <option>26</option>
                                                                        <option>27</option>
                                                                        <option>28</option>
                                                                        <option>29</option>
                                                                        <option>30</option>
                                                                        <option>31</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 form-group">
                                                                <div class="b-month">
                                                                    <select class="form-control">
                                                                        <option>January</option>
                                                                        <option>February</option>
                                                                        <option>March</option>
                                                                        <option>April</option>
                                                                        <option>May</option>
                                                                        <option>June</option>
                                                                        <option>July</option>
                                                                        <option>August</option>
                                                                        <option>September</option>
                                                                        <option>October</option>
                                                                        <option>November</option>
                                                                        <option>December</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 form-group">
                                                                <div class="b-year">
                                                                    <select class="form-control">
                                                                        <option>1970</option>
                                                                        <option>1971</option>
                                                                        <option>1972</option>
                                                                        <option>1973</option>
                                                                        <option>1974</option>
                                                                        <option>1975</option>
                                                                        <option>1976</option>
                                                                        <option>1977</option>
                                                                        <option>1978</option>
                                                                        <option>1979</option>
                                                                        <option>1980</option>
                                                                        <option>1981</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 full-width-xs">
                                                    <button type="submit" class="btn btn-send-email sign-up">SAVE</button>
                                                </div>
                                            </div>      
                                        </form>                                        
                                    </div>
                                </div> 

                                <button type="" class="btn btn-change-password" data-target="#btnChangePassword"  data-toggle="modal">CHANGE PASSWORD</button>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">
                <h2 class="heading-title row-fluid">ACCOUNT INFORMATION</h2>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>

    </div>    


    <!-- Modal Change Password -->
    <div class="modal fade " id="btnChangePassword" tabindex="-1" role="dialog" aria-labelledby="btnChangePassword" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="" style="float:left; line-height: 35px;">CHANGE PASSWORD</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="">Current Password</label>
                        <input type="password" class="input-control" id="" aria-describedby="" placeholder="...">
                    </div>
                    <div class="form-group">
                        <label for="">New Password</label>
                        <input type="password" class="input-control" id="" aria-describedby="" placeholder="...">
                    </div>
                    <div class="form-group">
                        <label for="">Confirm Password</label>
                        <input type="password" class="input-control" id="" aria-describedby="" placeholder="...">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary">Save</button>
            </div>
            </div>
        </div>
    </div>




    <?php include "footer.php"; ?>

</body>
</html>