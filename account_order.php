<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li>          
                    <li><a title="MY ORDER" class="active" href="account_order.php">MY ORDER</a></li>        
                </ol>
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">                
                <h1 class="heading-title row-fluid">MY ORDER</h1>
            </div>
        </section>

        <section class="row-fluid panel-padding-half">
            <div class="container">
                <div class="row-fluid form-group">
                    <div class="row">

                        <?php include "accound_aside.php";?>

                        <div class="col-lg-9 col-sm-9 form-group account-wrapper">
                            <div class="account-content">
                                <div class="element-wrap">                                    
                                    <h2 class="content-title">RECENT ORDERS</h2>
                                    <div class="row-fluid">
                                        <div class="table-responsive table-custom">    
                                            <div class="tb-outer">
                                                <div class="tb-header">
                                                    <ul>
                                                        <li class="pd-id hide-xs">ID</li>
                                                        <li>DATE</li>
                                                        <li>ORDER#</li>
                                                        <li class="pd-code hide-xs">PRODUCT CODE</li>
                                                        <li>PRICE</li>
                                                        <li class="hide-xs">SHIP TO</li>
                                                        <li>STATUS</li>
                                                    </ul>
                                                </div>
                                                <div class="tb-body">                                                
                                                    <a href="pending.php" class="order-list">
                                                        <ul>
                                                            <li class="pd-id hide-xs">1</li>
                                                            <li>09-06-2561</li>
                                                            <li>000001</li>
                                                            <li class="pd-code hide-xs">KU895u1</li>
                                                            <li>250.00</li>
                                                            <li class="hide-xs">BIG HOME FURNISH LAOS COUNTRY</li>
                                                            <li>PENDING</li>                                                        
                                                        </ul>
                                                    </a>
                                                    <?php for ($i=2; $i<=9 ; $i++) {?>
                                                        <a href="account_orderdetail.php" class="order-list">
                                                            <ul>
                                                                <li class="pd-id hide-xs"><?php echo "$i"; ?></li>
                                                                <li>09-06-2561</li>
                                                                <li>00000<?php echo "$i"; ?></li>
                                                                <li class="pd-code hide-xs">KU895u<?php echo "$i"; ?></li>
                                                                <li>250.00</li>
                                                                <li class="hide-xs">BIG HOME FURNISH LAOS COUNTRY</li>
                                                                <li>TRANSPORT</li>                                                        
                                                            </ul>
                                                        </a>
                                                    <?php } ?>
                                                    <a href="account_orderdetail.php" class="order-list">
                                                        <ul>
                                                            <li class="pd-id hide-xs">10</li>
                                                            <li>09-06-2561</li>
                                                            <li>0000010</li>
                                                            <li class="pd-code hide-xs">KU895u10</li>
                                                            <li>250.00</li>
                                                            <li class="hide-xs">BIG HOME FURNISH LAOS COUNTRY</li>
                                                            <li>RECEIVED</li>                                                        
                                                        </ul>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div aria-label="Page navigation example " class="pagi-custom">
                                            <ul class="pagination justify-content-center">
                                                <li class="page-item disabled">
                                                <a class="page-link" href="#" tabindex="-1">Previous</a>
                                                </li>
                                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                <li class="page-item">
                                                <a class="page-link" href="#">Next</a>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">
                <h2 class="heading-title row-fluid">MY ORDER</h2>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>

    </div>    

    <?php include "footer.php"; ?>

</body>
</html>