<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li>          
                    <li><a title="CART" class="active" href="cart.php">CART</a></li>        
                </ol>
            </div>
        </section>


        <section class="row-fluid"> 
            <div class="container">                
                <h1 class="heading-title row-fluid">CART</h1>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:20px;"></div>
        <section class="row-fluid">
            <div class="container">
                <div class="row-fluid cart-wrapper">
                    <div class="row-fluid cart-header">
                        <ul>
                            <li>PRODUCT</li>
                            <li>DETAIL</li>
                            <li>PRICE</li>
                            <li>QTY</li>
                            <li>SUBTOTLE</li>
                            <li>DELETE</li>
                        </ul>
                    </div>
                    <div class="row-fluid cart-body">
                        <?php for ($x=0; $x<=3 ; $x++) {?>
                            <?php for ($i=1; $i<=3 ; $i++) {?>
                                <div class="cart-body-inner">
                                    <div class="cart-img">
                                        <a class="" href="productdetail.php" >
                                            <img alt="" class="" src="images/product-<?php echo "$i"; ?>.png">
                                        </a>
                                    </div>
                                    <div class="cart-desc">
                                        <a class="" href="productdetail.php" title="Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an">
                                            <span class="text text-black" >
                                                Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an 
                                            </span>
                                            <span class="ID text-gray">UK89456</span>
                                        </a>
                                    </div>
                                    <div class="cart-price">
                                        <span>2,500</span>
                                    </div>
                                    <div class="cart-qty">
                                        <div class="qty-group">
                                            <div class="FormUnit FormUnit--spin js_spin">
                                                <input class="FormUnit-field FormUnit-field--spin js_spin-input" type="number" min="0" max="20" step="1" value="1" name="quantity" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cart-subtotal">
                                        <span>2,500</span>
                                    </div>                        
                                    <div class="cart-del" data-toggle="modal" data-target="#exampleModalDel">
                                        <i class="material-icons">delete_forever</i>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>  
                    <div class="row-fluid form-group btn-group-bottom">
                        <a href="checkout.php" class="btn btn-send-email">CHECKOUT</a> 
                        <button type="submit" class="btn btn-send-email clear-cart" data-toggle="modal" data-target="#exampleModalDelAll"><i class="material-icons">delete_forever</i>EMPTY CART</button>                   
                    </div>  
                </div>
            </div>
        </section>
        <div class="row-fluid empty-space" style="margin-top:20px;"></div>

        <section class="row-fluid"> 
            <div class="container">
                <h2 class="heading-title row-fluid">CART</h2>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>

    </div>    

    <?php include "footer.php"; ?>

    <script>
        // QTY
        (function (win, doc) {
            'use strict';
            if (!doc.querySelector || !win.addEventListener || !doc.documentElement.classList) {
                return;
            }

            var Spinner = function(rootElement) {
                //Add button selector
                var addButtonSelector = '.js_spin-add';
                //Remove button selector
                var removeButtonSelector = '.js_spin-remove';
                //Number input selector
                var numberInputSelector = '.js_spin-input';
                //A variable to store the markup for the add button
                var addButtonMarkup = '<button type="button" class="FormUnit-quantity FormUnit-quantity--add js_spin-add Icon Icon--isClosed"><i class="material-icons">add</i></button>';
                //A variable to store the markup for the remove button
                var removeButtonMarkup = '<button type="button" class="FormUnit-quantity FormUnit-quantity--remove js_spin-remove Icon Icon--remove"><i class="material-icons">remove</i></button>';
                //Variable to store the root's container
                var container;
                //A variable for the markup of the number input pattern
                var markup;
                //A variable to store a number input
                var numberInput;
                //Variable to store the add button
                var addButton;
                //Variable to store the remove button
                var removeButton;
                //Store max value
                var maxValue;
                //Store min value
                var minValue;
                //Store step value
                var step;
                //Store new value
                var newValue;
                //Variable to store the loop counter
                var i;

                //Initialisation function
                this.init = function() {
                    container = rootElement;
                    //Get the markup inside the number input container
                    markup = container.innerHTML;
                    //Create a button to decrese the value by 1
                    markup += removeButtonMarkup;
                    //Create a button to increase the value by 1
                    markup += addButtonMarkup;
                    //Update the container with the new markup
                    container.innerHTML = markup;

                    //Get the add and remove buttons
                    addButton = rootElement.querySelector(addButtonSelector);
                    removeButton = rootElement.querySelector(removeButtonSelector);
                    
                    //Get the number input element
                    numberInput = rootElement.querySelector(numberInputSelector);

                    //Get min, max and step values
                    if (numberInput.hasAttribute('max')) {
                        maxValue = parseInt(numberInput.getAttribute('max'), 10);
                    } else {
                        maxValue = 99999;
                    }
                    if (numberInput.hasAttribute('min')) {
                        minValue = parseInt(numberInput.getAttribute('min'), 10);
                    } else {
                        minValue = 0;
                    }
                    if (numberInput.hasAttribute('step')) {
                        step = parseInt(numberInput.getAttribute('step'), 10);
                    } else {
                        step = 1;
                    }

                    //Change the number input type to text
                    numberInput.setAttribute('type', 'text');
                    
                    //If there is there no pattern attribute, set it to only accept numbers
                    if (!numberInput.hasAttribute('pattern')) {
                        numberInput.setAttribute('pattern', '[0-9]');
                    }

                    //Add click events to the add and remove buttons
                    addButton.addEventListener('click', add, false);  
                    removeButton.addEventListener('click', remove, false);
                };

                //Methods for setting values
                this.setAddButtonMarkup = function(markup) {
                    addButtonMarkup = markup;
                };

                this.setRemoveButtonMarkup = function(markup) {
                    removeButtonMarkup = markup;
                };

                this.setAddButtonSelector = function(selector) {
                    addButtonSelector = selector;
                };

                this.setRemoveSelector = function(selector) {
                    removeButtonSelector = selector;
                };

                this.setNumberInputSelector = function(selector) {
                    numberInputSelector = selector;
                };

                //Function to add one to the quantity value
                var add = function(ev) {
                    newValue = parseInt(numberInput.value, 10) + step;
                    //If the value is less than the max value
                    if (newValue <= maxValue) {
                        //Add one to the number input value
                        numberInput.value = newValue;
                        //Button is active
                        removeButton.disabled = false;
                    }
                    //If the value is equal to the max value
                    if (numberInput.value == maxValue || newValue > maxValue) {
                        //Disable the button
                        addButton.disabled = true;
                    }
                    ev.preventDefault();
                };
                //Function to subtract one from the quantity value
                var remove = function(ev) {
                    newValue = parseInt(numberInput.value, 10) - step;
                    //If the number input value is bigger than the min value, reduce the the value by 1
                    if (newValue >= minValue) {
                        numberInput.value = newValue;
                        addButton.disabled = false;
                    }
                    //If the input value is the min value, add disabled property to the button
                    if (numberInput.value == minValue || newValue < minValue) {
                        removeButton.disabled = true;
                    }
                    ev.preventDefault();
                };
            };

            //Get all of the number input elements
            var spins = doc.querySelectorAll('.js_spin');
            //Store the total number of number inputs
            var spinsTotal = spins.length;
            //A variable to store one number inputs
            var spin;
            //A counter for the loop
            var i;
            //Loop through each number input
            for ( i = 0; i < spinsTotal; i = i + 1 ) {
                //Create a new Spin object for each number input
                spin = new Spinner(spins[i]);
                //Start the initialisation function
                spin.init();
            }

        }(this, this.document));

    </script>
</body>
</html>