<section class="row-fluid">  
    <div class="container">
        <div class="categories-toggle-wrap">
            <a id="categories-toggle">
                MENU<i class="material-icons"></i>
            </a> 
        </div>
        <div class="categories row-fluid">
            <ul>
                <li >
                    <a class="active">ALL PRODUCT</a>
                </li>
                <li>
                    <!-- <a href="search.php">BEDROOM</a> -->
                    <a class="link plain-text dropdown-toggle" id="dropdown-bedroom" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        BEDROOM
                    </a>
                    <div class="dropdown-menu dropdown-cat-menu" aria-labelledby="dropdown-bedroom">
                        <?php for ($i=0; $i<=1 ; $i++) {?>
                            <ul class="dropdown-sub-menu">
                                <li>
                                    <a href="search.php" class="">BEDROOM 1</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">BEDROOM 2</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">BEDROOM 3</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">BEDROOM 4</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">BEDROOM 5</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">BEDROOM 6</a>
                                </li>                                    
                            </ul>
                        <?php } ?>
                    </div>
                </li>
                <li>
                    <a class="link plain-text dropdown-toggle" id="dropdown-livingroom" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        LIVING ROOM
                    </a>
                    <div class="dropdown-menu dropdown-cat-menu" aria-labelledby="dropdown-livingroom">
                        <?php for ($i=0; $i<=1 ; $i++) {?>
                            <ul class="dropdown-sub-menu">
                                <li>
                                    <a href="search.php" class="">LIVING ROOM 1</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">LIVING ROOM 2</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">LIVING ROOM 3</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">LIVING ROOM 4</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">LIVING ROOM 5</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">LIVING ROOM 6</a>
                                </li>                                    
                            </ul>
                        <?php } ?>
                    </div>
                </li>
                <li><a href="search.php">KITCHEN</a></li>
                <li><a href="search.php">DINING ROOM</a></li>
                <li>
                    <a class="link plain-text dropdown-toggle" id="dropdown-homeamdliving" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        HOME AND LIVING
                    </a>
                    <div class="dropdown-menu dropdown-cat-menu" aria-labelledby="dropdown-homeamdliving">
                        <?php for ($i=0; $i<=1 ; $i++) {?>
                            <ul class="dropdown-sub-menu">
                                <li>
                                    <a href="search.php" class="">HOME AND LIVING 1</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">HOME AND LIVING 2</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">HOME AND LIVING 3</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">HOME AND LIVING 4</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">HOME AND LIVING 5</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">HOME AND LIVING 6</a>
                                </li>                                    
                            </ul>
                        <?php } ?>
                    </div>
                </li>
                <li>
                    <!-- <a href="search.php"> HOME DECOR </a> -->
                    <a class="link plain-text dropdown-toggle" id="dropdown-homedecore" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        HOME DECOR
                    </a>
                    <div class="dropdown-menu dropdown-cat-menu" aria-labelledby="dropdown-homedecore">
                        <?php for ($i=0; $i<=1 ; $i++) {?>
                            <ul class="dropdown-sub-menu">
                                <li>
                                    <a href="search.php" class="">HOME DECOR 1</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">HOME DECOR 2</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">HOME DECOR 3</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">HOME DECOR 4</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">HOME DECOR 5</a>
                                </li>
                                <li>
                                    <a href="search.php" class="">HOME DECOR 6</a>
                                </li>                                    
                            </ul>
                        <?php } ?>
                    </div>
                </li>
                <li><a href="search.php">OUTDOOR</a></li>
            </ul>
        </div>
    </div>
</section>