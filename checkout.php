<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li>          
                    <li><a title="CHECKOUT" class="active" href="confirm.php">CHECKOUT</a></li>        
                </ol>
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">                
                <h1 class="heading-title row-fluid">CHECKOUT</h1>
            </div>
        </section>

        <section class="row-fluid panel-padding-half">
            <div class="container">

                <div class="row-fluid checkout-panel">
                    <div class="row">
                        <div class="col-md-4 full-width-xs">
                            <div class="billing-wrap">
                                <h6 class="row-fluid form-group bold">DEFAULT SHIPPING ADDRESS</h6>
                                <div class="address-panel">
                                    <form class="address-form">
                                        <input type="text" name="" class="input-control form-group" placeholder="*FIRST NAME">
                                        <input type="text" name="" class="input-control form-group" placeholder="*LAST NAME">
                                        <input type="text" name="" class="input-control form-group" placeholder="COMPANY">
                                        <input type="text" name="" class="input-control form-group" placeholder="*MOBILE">
                                        <input type="text" name="" class="input-control form-group" placeholder="FAX">
                                        <input type="text" name="" class="input-control form-group" placeholder="TELEPHONE">
                                        <input type="text" name="" class="input-control form-group" placeholder="*STREET ADDRESS">
                                        <input type="text" name="" class="input-control form-group" placeholder="*STATE I PROVINCE">
                                        <input type="text" name="" class="input-control form-group" placeholder="*CITY">
                                        <input type="text" name="" class="input-control form-group" placeholder="*SUB DISTRICT">
                                        <input type="text" name="" class="input-control form-group" placeholder="*ZIPCODE">
                                        <input type="text" name="" class="input-control form-group" placeholder="*COUNTRY">
                                    </form>
                                    <div class="select-state">
                                        <div class="row-fluid form-group select-address origin-address active">
                                            <i class="material-icons icons">lens</i>
                                            <span>BILLING TO THIS ADDRESS</span>
                                        </div>
                                        <div class="row-fluid form-group select-address diff-address">
                                            <i class="material-icons icons">lens</i>
                                            <span>BILLING TO DIFFERENT ADDRESS</span>
                                        </div>
                                    </div>
                                </div>                            
                                <div class="origin-address-detail active">
                                    <h6 class="row-fluid form-group bold">DEFAULT BILLING ADDRESS</h6>
                                    <div class="address-panel">
                                        <span class="row-fluid text plain-text">
                                            TOSAPOL CHUENMALA<br>
                                            120/34-35 MOO 24<br>
                                            SILA SUB DISTRICT, <br>
                                            MUEANG KHONKAEN DISTRICT, <br>
                                            KHONKAEN PROVINCE. 40000, THAILAND
                                        </span>
                                    </div>
                                </div>
                                <div class="diff-address-detail">
                                    <h6 class="row-fluid form-group bold">DIFFERENT BILLING ADDRESS</h6>
                                    <div class="row-fluid">
                                        <div class="address-panel">
                                            <form class="address-form">
                                                <input type="text" name="" class="input-control form-group" placeholder="*FIRST NAME">
                                                <input type="text" name="" class="input-control form-group" placeholder="*LAST NAME">
                                                <input type="text" name="" class="input-control form-group" placeholder="COMPANY">
                                                <input type="text" name="" class="input-control form-group" placeholder="*MOBILE">
                                                <input type="text" name="" class="input-control form-group" placeholder="FAX">
                                                <input type="text" name="" class="input-control form-group" placeholder="TELEPHONE">
                                                <input type="text" name="" class="input-control form-group" placeholder="*STREET ADDRESS">
                                                <input type="text" name="" class="input-control form-group" placeholder="*STATE I PROVINCE">
                                                <input type="text" name="" class="input-control form-group" placeholder="*CITY">
                                                <input type="text" name="" class="input-control form-group" placeholder="*SUB DISTRICT">
                                                <input type="text" name="" class="input-control form-group" placeholder="*ZIPCODE">
                                                <input type="text" name="" class="input-control form-group" placeholder="*COUNTRY">
                                            </form>
                                        </div>
                                    </div>
                                    <div class="row-fluid address-panel select-billing">  
                                        <label class="control control--radio">
                                            <input type="radio" name="radio2">
                                            <div class="control__indicator"></div>
                                        </label>                         
                                        <div class="row-fluid form-group">  
                                            <p>
                                                MISS KANKANIT KONGTHONG<br>
                                                120/34-35 MOO 24<br>
                                                SILA SUB DISTRICT, <br>
                                                MUEANG KHONKAEN DISTRICT,<br> 
                                                KHONKAEN PROVINCE. 40000, <br>
                                                THAILAND
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row-fluid address-panel select-billing"> 
                                        <label class="control control--radio">
                                            <input type="radio" name="radio2">
                                            <div class="control__indicator"></div>
                                        </label>                          
                                        <div class="row-fluid form-group">  
                                            <p>
                                                MISS KANKANIT KONGTHONG<br>
                                                120/34-35 MOO 24<br>
                                                SILA SUB DISTRICT, <br>
                                                MUEANG KHONKAEN DISTRICT,<br> 
                                                KHONKAEN PROVINCE. 40000, <br>
                                                THAILAND
                                            </p>
                                        </div>
                                    </div>     
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 full-width-xs">
                            <h6 class="row-fluid form-group bold">SHIPPING METHOD</h6>
                            <div class="address-panel">
                                <span class="row-fluid text plain-text">
                                    <p class="pull-left">DELIVERY TIME </p>
                                    <p class="pull-right">5 DAYS</p>
                                </span>
                                <span class="row-fluid text plain-text">
                                    <p class="pull-left"> SHIPPING FEE </p>
                                    <p class="pull-right">200 .-</p>
                                </span>
                                <span class="row-fluid text plain-text text-yellow">*STAFF CALL AGAIN ON DELIVERY DATE</span>
                            </div>
                            <h6 class="row-fluid form-group bold">PAYMENT</h6>
                            <div class="address-panel payment-method-selectd">
                                <div class="payment-method">
                                    <div class="row-fluid form-group select-method credit-method">
                                        <div id="credit" class="row-fluid form-group credit  active">
                                            <i class="material-icons icons">lens</i>
                                            <span class="plain-text">CREDIT / DEBIT CARD</span>
                                        </div>
                                        <div class="credit-method-img">
                                            <ul>
                                                <li>
                                                    <a href="" target="_blank"><img src="images/VISA.png" /></a>
                                                </li>
                                                <li>
                                                    <a href="" target="_blank"><img src="images/mastercard2.jpg" /></a>
                                                </li>
                                                <li>
                                                    <a href="" target="_blank"><img src="images/JCB_logo.svg.png" /></a>
                                                </li>
                                                <li>
                                                    <a href="" target="_blank"><img src="images/American-Express-Vector-Logo.jpg" /></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row-fluid form-group select-method bank-method">
                                        <div id="bank" class="row-fluid form-group bank ">
                                            <i class="material-icons icons">lens</i>
                                            <span class="plain-text">BANK TRANSFER</span>
                                        </div>
                                        <div class="bank-method-img">
                                            <ul>
                                                <li>
                                                    <img src="images/k-bank.jpg" />
                                                    <span>302-15-13-789</span>
                                                </li>
                                                <li>
                                                    <img src="images/scb-bank.png" />
                                                    <span>302-15-13-789</span>
                                                </li>
                                                <li>
                                                    <img src="images/tmb-bank.png" />
                                                    <span>302-15-13-789</span>
                                                </li>
                                                <li>
                                                    <img src="images/gsb-bsnk.jpg" />
                                                    <span>302-15-13-789</span>
                                                </li>
                                                <li>
                                                    <img src="images/ktbthai-bank.jpg" />
                                                    <span>302-15-13-789</span>
                                                </li>
                                                <li>
                                                    <img src="images/tnc-bank.jpg" />
                                                    <span>302-15-13-789</span>
                                                </li>
                                                <li>
                                                    <img src="images/krungsri-bank.jpg" />
                                                    <span>302-15-13-789</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid empty-space" style="margin-top:50px;"></div>
                                <span class="row-fluid text plain-text bold">CALL CENTER : 02-2356547</span>
                            </div>
                        </div>
                        <div class="col-md-4 full-width-xs">
                            <h6 class="row-fluid form-group bold">REVIEW</h6>
                            <div class="row-fluid panel-review-wrap">
                                <div class="panel-review-inner panel-scroll">
                                    <?php for ($i=0; $i<=4 ; $i++) {?>
                                        <?php for ($x=1; $x<=3 ; $x++) {?>
                                            <div class="row-fluid panel-review-list">
                                                <div class="images">
                                                    <img class="" src="images/product-<?php echo "$x"; ?>.png?v=1">
                                                </div>
                                                <div class="desc">
                                                    <div class="desc-body">
                                                        <span class="text text-black">
                                                            Alienum phaedrum torquatos nec eu, vis detraxit 
                                                            periculis ex, nihil expetendis in mei. Mei an
                                                        </span>
                                                    </div>
                                                    <div class="desc-footer">
                                                        <span class="pull-left desc price">2,500 .- </span>
                                                        <span class="pull-right desc amount">2</span>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="address-panel">
                                <span class="row-fluid text plain-text">
                                    <p class="pull-left">SUBTOTAL </p>
                                    <p class="pull-right">25,000 .-</p>
                                </span>
                                <span class="row-fluid text plain-text">
                                    <p class="pull-left"> DELIVERY TIME</p>
                                    <p class="pull-right">5 DAYS</p>
                                </span>
                                <span class="row-fluid text plain-text">
                                    <p class="pull-left"> SHIPPING FEE </p>
                                    <p class="pull-right">200 .-</p>
                                </span>
                                <span class="row-fluid text plain-text">
                                    <p class="pull-left bold"> TOTAL (VAT) </p>
                                    <p class="pull-right">25,200 .-</p>
                                </span>
                            </div>
                            <h6 class="row-fluid form-group bold">COMMENT</h6>
                            <div class="address-panel">
                                <form class="comment-form">
                                    <input type="text" name="" class="input-control" placeholder="COMMENT">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>   
                <div class="row-fluid form-group btn-group-bottom">
                    <a class="btn btn-default btn-previous" href="cart.php">PREVIOUS</a>   
                    <a class="btn btn-default btn-next" href="checkout_verify.php">PLACE ORDER</a>         
                </div>  
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">
                <h2 class="heading-title row-fluid">CHECKOUT</h2>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>

    </div>    

    <?php include "footer.php"; ?>

</body>
</html>