<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li> 
                    <li><a title="CHECKOUT" href="checkout.php">CHECKOUT</a></li>         
                    <li><a title="CHECKOUT VERIFY" href="checkout_verify.php">CHECKOUT VERIFY</a></li>              
                    <li><a title="CONFIRM INFORMATION" class="active" href="checkout_confirm.php">CONFIRM INFORMATION</a></li>        
                </ol>
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">                
                <h1 class="heading-title row-fluid">CONFIRM INFORMATION</h1>
            </div>
        </section>

        <section class="row-fluid panel-padding-half">
            <div class="container">
                <div class="row-fluid box-bg confirm-info">
                    
                    <div class="row-fluid form-group">
                        <i class="material-icons mood-icons">mood</i>
                        <h1 class="confirm-heading">YOUR ORDER HAS BEEN RECEIVED THANK YOU FOR YOUR PURCHASE !</h1>
                    </div>
                    <div class="row-fluid form-group">
                        <span class="row-fluid">YOUR ORDER # IS : <span class="text-yellow">19870025</span></span>
                        <span>
                            YOU WILL RECIEVE AN ORDER CON RMATION EMAIL WITH DETAILS OF YOUR ORDERS AND I LINK TO TAG IT'S PROGRESS.
                            IF YOU MAKE YOUR PAYMENT. PLEASE <a class="link text-yellow" href="">CLICK HERE</a> IN ORDER TO CONFIRM THE PAYMENT
                            <a class="link text-yellow" href="">CLICK HERE</a>TO PRINT A COPY YOUR ORDER CON RMATION
                        </span>
                    </div>
                    <a class="btn btn-send-email" href="index.php">CONTINUE SHOPPING</a>                    
                </div>               
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">
                <h2 class="heading-title row-fluid">CONFIRM INFORMATION</h2>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>

    </div>    

    <?php include "footer.php"; ?>

</body>
</html>