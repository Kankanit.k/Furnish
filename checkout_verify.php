<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li> 
                    <li><a title="CHECKOUT" href="checkout.php">CHECKOUT</a></li>         
                    <li><a title="CHECKOUT VERIFY" class="active" href="checkout_verify.php">CHECKOUT VERIFY</a></li>        
                </ol>
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">                
                <h1 class="heading-title row-fluid">CHECKOUT VERIFY</h1>
            </div>
        </section>

        <section class="row-fluid ">
            <div class="container">
                <div class="row-fluid form-group">
                    <div class="row-fluid empty-space" style="margin-top:50px;"></div>
                    <div class="pull-left">
                        <span class="plain-text bold">#12345900AFT</span><br>
                        <span class="plain-text">ORDER DATE : 06/06/2561</span><br>
                        <span class="plain-text">STATUS : PENDING</span>
                    </div>
                    <a href="invoice-print.php" target="_blank" class="btn btn-print pull-right">
                        <i class="fa fa-print"></i>
                        PRINT ORDER
                    </a>
                    <div class="row-fluid empty-space" style="margin-top:50px;"></div>
                </div>

                <div class="row-fluid cart-wrapper cart-verify">
                    <div class="row-fluid cart-header">
                        <ul>
                            <li>PRODUCT</li>
                            <li>DETAIL</li>
                            <li>PRICE</li>
                            <li>QTY</li>
                            <li>SUBTOTLE</li>
                        </ul>
                    </div>
                    <div class="row-fluid cart-body">
                        <?php for ($i=1; $i<=3 ; $i++) {?>
                            <div class="cart-body-inner">
                                <div class="cart-img">
                                    <a class="" href="productdetail.php" title="Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an">
                                        <img alt="" class="" src="images/product-<?php echo "$i"; ?>.png">
                                    </a>
                                </div>
                                <div class="cart-desc">
                                    <a class="" href="productdetail.php" title="Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an">
                                        <span class="text text-black" >
                                            Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an 
                                        </span>
                                        <span class="ID text-gray">UK89456</span>
                                    </a>
                                </div>
                                <div class="cart-price">
                                    <span>2,500</span>
                                </div>
                                <div class="cart-qty">
                                    <span class="total-amount">2</sapn>
                                </div>
                                <div class="cart-subtotal">
                                    <span>2,500</span>
                                </div>      
                            </div>
                        <?php } ?>
                    </div> 
                    
                    <div class="row-fluid form-group">
                        <div class="row">                           
                            <div class="col-md-4 full-width-xs">
                                <h6 class="row-fluid form-group bold">SHIPPING ADDRESS</h6>
                                <div class="address-panel">
                                    <span class="row-fluid text plain-text">
                                        TOSAPOL CHUENMALA<br>
                                        120/34-35 MOO 24<br>
                                        SILA SUB DISTRICT, <br>
                                        MUEANG KHONKAEN DISTRICT, <br>
                                        KHONKAEN PROVINCE. <br>
                                        40000,<br>
                                         THAILAND
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4 full-width-xs">    
                                <div class="row-fluid">
                                    <h6 class="row-fluid form-group bold">BILLING ADDRESS 1</h6>
                                    <div class="address-panel">
                                        <span class="row-fluid text plain-text">
                                            TOSAPOL CHUENMALA<br>
                                            120/34-35 MOO 24<br>
                                            SILA SUB DISTRICT, <br>
                                            MUEANG KHONKAEN DISTRICT, <br>
                                            KHONKAEN PROVINCE.<br>
                                            40000,<br>
                                            THAILAND
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 full-width-xs">
                                <h6 class="row-fluid form-group bold">TOTAL</h6>
                                <div class="address-panel">
                                    <span class="row-fluid text plain-text">
                                        <p class="pull-left">SUBTOTAL </p>
                                        <p class="pull-right">25,000 .-</p>
                                    </span>
                                    <span class="row-fluid text plain-text">
                                        <p class="pull-left"> DELIVERY TIME</p>
                                        <p class="pull-right">5 DAYS</p>
                                    </span>
                                    <span class="row-fluid text plain-text">
                                        <p class="pull-left"> SHIPPING FEE </p>
                                        <p class="pull-right">200 .-</p>
                                    </span>
                                    <span class="row-fluid text plain-text">
                                        <p class="pull-left bold"> TOTAL (VAT) </p>
                                        <p class="pull-right">25,200 .-</p>
                                    </span>
                                </div>
                                <h6 class="row-fluid form-group bold">PAYMENT METHOD</h6>                                
                                <div class="row-fluid ">
                                    <div class="address-panel form-group">
                                        <span class="text text-black">BANK TRANSFER</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div> 
                <div class="row-fluid form-group btn-group-bottom">
                    <a class="btn btn-default btn-previous" href="checkout.php">PREVIOUS</a>   
                    <a class="btn btn-default btn-next" href="checkout_confirm.php">CONFIRM</a>       
                </div>  
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">
                <h2 class="heading-title row-fluid">CHECKOUT VERIFY</h2>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>

    </div>    

    <?php include "footer.php"; ?>

</body>
</html>