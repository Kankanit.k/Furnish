<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <?php include "cat_menu.php"; ?>  

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li>          
                    <li><a title="CONTACT US" class="active" href="contact.php">CONTACT US</a></li>        
                </ol>               
            </div>
        </section>
        
        <?php include "search_box.php"; ?>        

        <section class="row-fluid"> 
            <div class="container">                
                <h1 class="heading-title row-fluid">CONTACT US</h1>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:70px;"></div>
        <section class="row-fluid panel-padding bg-softgray bg-img">
            <div class="container">
                <div class="logo-left">
                    <img alt="" class="" src="images/logo-01.png?v=1" />                    
                </div>
                <h2 class="heading-2">CONTACT US</h2>  
                <div class="row-fluid">
                    <span class="xl-large text-black">023-654-215</span><br>
                    <span class="sm-large text-brown">ON MON-FRI FROM 8:30 AM - 5:20 PM</span><br>
                    <span class="sm-large text-brown">(ACCEPTED ON FESTIVALS AND HOLIDAYS)</span><br><br><br>
                    <span class="sm-large text-black">BIG HOME@EMAIL.COM</span><br><br><br>
                    <span class="sm-large text-black">BIG HOME FURNISH</span><br>
                    <span class="sm-large text-black">NO. 127/12, SOI RAMA 2 SEC 50 ROAD, SAMEA DAM, BANG KHUN-THIAN, BANGKOK 10000 </span>      
                </div>        
            </div>
        </section>
        <div class="row-fluid empty-space" style="margin-top:50px;"></div>

        <section class="row-fluid"> 
            <div class="container">
                <h2 class="heading-title row-fluid">CONTACT US</h2>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>

    </div>    

    <?php include "footer.php"; ?>

</body>
</html>