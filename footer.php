<footer class="main-footer">
    <div class="wrapper">
        
        <div class="footer-top bg-gray">
            <div class=" container">  
                <div class="row">
                    <div class="col-md-4">
                        <div class="site-map">
                            <ul>
                                <li>
                                    <a class="link text-white" href="" title="BEDROOM">BEDROOM</a>
                                </li>
                                <li>
                                    <a class="link text-white" href="" title="LIVING ROOM">LIVING ROOM</a>
                                </li>
                                <li>
                                    <a class="link text-white" href="" title="KITCHEN">KITCHEN </a>
                                </li>
                                <li>
                                    <a class="link text-white" href="" title="DINING ROOM">DINING ROOM</a>
                                </li>
                                <li>
                                    <a class="link text-white" href="" title="HOME AND LIVING">HOME AND LIVING</a>
                                </li>
                                <li>
                                    <a class="link text-white" href="" title="HOME DECOR">HOME DECOR</a>
                                </li>
                                <li>
                                    <a class="link text-white" href="" title="OUTDOOR">OUTDOOR</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="site-map">
                            <ul>
                                <li>
                                    <a class="link text-white" href="promotion.php" title="PROMOTION">PROMOTION</a>
                                </li>
                                <li>
                                    <a class="link text-white" href="product.php" title="PRODUCT">PRODUCT</a>
                                </li>
                                <li>
                                    <a class="link text-white" href="gallery.php" title="GALLERY">GALLERY </a>
                                </li>
                                <li>
                                    <a class="link text-white" href="about.php" title="ABOUT US">ABOUT US</a>
                                </li>
                                <li>
                                    <a class="link text-white" href="contact.php" title="CONTACT">CONTACT</a>
                                </li>
                            </ul>  
                        </div>          
                    </div>
                    <div class="col-md-4">
                        <div class="site-map">
                            <ul>
                                <li>
                                    <span class="text-white">
                                        CALL<br>
                                        02-536-4521
                                    </span>
                                </li>                           
                            </ul>
                        </div>
                        <div class="social">
                            <ul>
                                <li>
                                    <a class="link text-white" target="_blank" href="https://www.facebook.com/">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="link text-white" target="_blank" href="https://twitter.com/">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="link text-white" target="_blank" href="https://www.youtube.com/">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </li>                                
                            </ul>
                        </div>
                    </div>
                </div>                 
            </div>
        </div>
        <div class="footer-bottom">
            <div class=" container">
                <center>
                    <span class="text-white plain-text">
                        <i class="material-icons icons">copyright</i>
                        2018 <a class="text-white link router-link" href=""> BIG HOME FURNISH.COM </a>
                        ALL RIGHT RESERVED. ENGINE BY <a class="text-white link router-link" href="https://www.wynnsoft-solution.com/"> WYNNSOFT SOLUTION CO,LTD. </a>
                    </span>
                </center>
            </div>
        </div>
    </div>
</footer>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script> -->
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script> -->

<!-- Date time  -->
<script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js'></script>


<!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js'></script> -->

<!-- Slick -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js'></script>



<script src="js/index.js"></script>
