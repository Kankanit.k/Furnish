<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li>          
                    <li><a title="FORGOT PASSWORD" class="active" href="forgotpassword.php">FORGOT PASSWORD</a></li>        
                </ol>
            </div>
        </section>

        <section class="row-fluid">  
            <div class="container">                
                <div class="main-content row-fluid">                    
                    <h1 class="heading-title row-fluid">FORGOT PASSWORD ?</h1>
                    <div class="row-fluid content-inner">
                        <center class="form-group">
                            <h4 class="heading-large row-fluid">FORGOT YOUR PASSWORD ?</h4>
                            <span class="plain-text">NO PROBLEM! JUST LL IN THE EMAIL BELOW AND WE'LL SEND YOUR PASSWORD RESET INTRODUCTIONS! </span>
                        </center>        
                        <form role="form" method="" class="form-sendemail">
                            <input type="email" name="" class="input-control form-group" placeholder="Your Email*" />
                            <button type="submit" class="btn btn-send-email">RESET PASSWORD</button>
                        </form>                
                    </div>
                    <h2 class="heading-title row-fluid">FORGOT PASSWORD ?</h2>
                </div>

            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>

    </div>    

    <?php include "footer.php"; ?>

</body>
</html>