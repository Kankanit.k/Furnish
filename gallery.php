<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <?php include "cat_menu.php"; ?>

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li>          
                    <li><a title="GALLERY" class="active" href="gallery.php">GALLERY</a></li>        
                </ol>                
            </div>
        </section>

        <?php include "search_box.php"; ?>

        <section class="row-fluid"> 
            <div class="container">   
                <div class="box-heading row-fluid">
                    <h1 class="title pull-left">GALLERY</h1>
                    <div class="right-group">
                        <div class="pagination">
                            <ul>
                                <li><a><i class="material-icons">first_page</i></a></li>
                                <li class="active"><a>1</a></li>
                                <li><a>2</a></li>
                                <li><a>3</a></li>
                                <li>...</li>
                                <li><a>60</a></li>
                                <li><a><i class="material-icons">last_page</i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:20px;"></div>
        <section class="row-fluid">
            <div class="container">

                <div class="row">  
                    <?php for ($i=0; $i<=2 ; $i++) {?>
                        <div class="col-md-3 form-group">
                            <a title="LOREM IPSUM DOLOR SIT AMET, EI NONUMY INCIDERINT VIM, VEL DICANT NUSQUAM TE" class="gall-card box-border" href="" >
                                <div class="image">
                                    <img alt="" class="" src="images/gall-1.jpg?v=1">
                                </div> 
                                <div class="desc">
                                    <span class="text">
                                        LOREM IPSUM DOLOR SIT AMET, EI NONUMY
                                        INCIDERINT VIM, VEL DICANT NUSQUAM TE
                                    </span>
                                    <span class="date">04/06/2561</span>
                                </div>
                            </a>     
                        </div>
                        <div class="col-md-3 form-group">
                            <a title="LOREM IPSUM DOLOR SIT AMET, EI NONUMY INCIDERINT VIM, VEL DICANT NUSQUAM TE" class="gall-card box-border" href="" >
                                <div class="image">
                                    <img alt="" class="" src="images/gall-2.jpg?v=1">
                                </div> 
                                <div class="desc">
                                    <span class="text">
                                        LOREM IPSUM DOLOR SIT AMET, EI NONUMY
                                        INCIDERINT VIM, VEL DICANT NUSQUAM TE
                                    </span>
                                    <span class="date">04/06/2561</span>
                                </div>
                            </a>     
                        </div>
                        <div class="col-md-3 form-group">
                            <a title="LOREM IPSUM DOLOR SIT AMET, EI NONUMY INCIDERINT VIM, VEL DICANT NUSQUAM TE" class="gall-card box-border" href="" >
                                <div class="image">
                                    <img alt="" class="" src="images/gall-3.jpg?v=1">
                                </div> 
                                <div class="desc">
                                    <span class="text">
                                        LOREM IPSUM DOLOR SIT AMET, EI NONUMY
                                        INCIDERINT VIM, VEL DICANT NUSQUAM TE
                                    </span>
                                    <span class="date">04/06/2561</span>
                                </div>
                            </a>     
                        </div>
                        <div class="col-md-3 form-group">
                            <a title="LOREM IPSUM DOLOR SIT AMET, EI NONUMY INCIDERINT VIM, VEL DICANT NUSQUAM TE" class="gall-card box-border" href="" >
                                <div class="image">
                                    <img alt="" class="" src="images/gall-4.jpg?v=1">
                                </div>
                                <div class="desc">
                                    <span class="text">
                                        LOREM IPSUM DOLOR SIT AMET, EI NONUMY
                                        INCIDERINT VIM, VEL DICANT NUSQUAM TE
                                    </span>
                                    <span class="date">04/06/2561</span>
                                </div>
                            </a>     
                        </div>
                    <?php } ?>
                </div>     
            </div>
        </section>
        <div class="row-fluid empty-space" style="margin-top:20px;"></div>

        <section class="row-fluid"> 
            <div class="container">
                <div class="box-heading row-fluid">
                    <h5 class="title pull-left">GALLERY</h5>
                    <div class="right-group">
                        <div class="pagination">
                            <ul>
                                <li><a><i class="material-icons">first_page</i></a></li>
                                <li class="active"><a>1</a></li>
                                <li><a>2</a></li>
                                <li><a>3</a></li>
                                <li>...</li>
                                <li><a>60</a></li>
                                <li><a><i class="material-icons">last_page</i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>

    </div>    

    <?php include "footer.php"; ?>

</body>
</html>