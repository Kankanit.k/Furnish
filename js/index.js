// -------------
// window.onclick = function(event){
//   if (document.getElementsByClassName('.dropdown-toggle')[0].contains(event.target)){
//     $('.dropdown-menu.dropdown-large').toggleClass('show');

//   }else{
//     $('.dropdown-menu.dropdown-large').removeClass('show');
//   }
// };

// $('.dropdown-toggle').click(function(){  
//   $('.dropdown-menu.dropdown-large').toggleClass('show');
// });


// -------------
$('.language-list').click(function(){
	$('.language-list.active').removeClass('active');
	$(this).addClass('active');
});

// Bank
$('.bank-list').click(function(){
    $('.bank-list.active').removeClass('active');
    $(this).addClass('active');
});

// Account aside menu
$('.account-aside-menu > ul > li').click(function(){
  $('.account-aside-menu > ul > li.active').removeClass('active');
  $(this).addClass('active');
});

// -- Aside Toggle
$('#aside-toggle').click(function(){
  $('.account-aside-menu').slideToggle('300');
});

// -- Categories Toggle
$('#categories-toggle').click(function(){
  $('.categories').slideToggle('300');
});

// Aside Filter
$('.aside-mobile-header').click(function(){
  $('.aside').toggleClass('active');
});

// -------------
$('#toggle').click(function(){
	$('body').addClass('toggled');
	$('body.toggled .navbar-overlay').removeClass('fadeOut animated').addClass('fadeIn animated');
  $('.navbar').addClass('fullHeight');
});
$('.navbar-overlay').click(function(){
	$('body').removeClass('toggled');
   $('.navbar').removeClass('fullHeight');
	$(this).removeClass('fadeIn').addClass('fadeOut');
});

// --------------
$('.modal-show').click(function(e){
	var modalID = $(this).data("target");
	$(modalID).show();
    $('body').addClass('active'); 
});
$('.icon-closemodal').click(function(e){
	$(".bg-black").hide();
    $('body').removeClass('active'); 
});

// --------------
$('.origin-address').click(function(){
    $(this).addClass('active');
    $('.diff-address').removeClass('active');
    $('.billing-wrap').find('.origin-address-detail').addClass('active');
    $('.billing-wrap').find('.diff-address-detail').removeClass('active');
});
$('.diff-address').click(function(){
    $(this).addClass('active');
    $('.origin-address').removeClass('active');
    $('.billing-wrap').find('.diff-address-detail').addClass('active');
    $('.billing-wrap').find('.origin-address-detail').removeClass('active');
});

// --------------
$('#credit').click(function(){
    $('.credit').addClass('active');
    $('.bank').removeClass('active');
});
$('#bank').click(function(){
    $('.bank').addClass('active');
    $('.credit').removeClass('active');
});

// -------------- Back to top 
$(window).scroll( function(){  
    if ($(window).scrollTop() > 100) {
        $('#top-link-block').css({'display':'block','-webkit-transition':'all 3.0s ease 0s'});
    } 
    else if ($(window).scrollTop() < 100){   
         $('#top-link-block').css({'display':'none','-webkit-transition':'all 3.0s ease 0s'});        
    }
    else{   
         $('#top-link-block').css({'display':'block','-webkit-transition':'all 3.0s ease 0s'});        
    } 
});

// -------------- QTY
$('.quantity-up').on('click', function(){
    var elem = $(this).closest('.quantity').find('.quantity-val');
    var val = Number(elem.val())+1;
    elem.val(val);
});

// --------------
$('.quantity-down').on('click', function(){
    var elem = $(this).closest('.quantity').find('.quantity-val');
    var val = Number(elem.val())-1;
    if (val < 1) val = 1;
    elem.val(val);
});

// -------------- Color
$('.color').click(function(){
    $('.color.active').removeClass('active');
    $(this).addClass('active');
});

// -------------- Slider
$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: true,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    autoplay: true,
    autoplaySpeed: 2000,
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 2
          }
        }
    ]
});

// Content-slick --
$(".related-slick").slick({
  dots: false,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,

  // the magic
    responsive: [{

      breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          autoplay: true,
          dots: true,
        }

      }, {

      breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,     
          autoplay: true,  
          dots: true,  
        }

      }, {

      breakpoint: 481,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: true,
          dots: true,         
        }

      }, {

      breakpoint: 320,
        settings: "unslick" // destroys slick

      }]
});



// -- Upload File 
$(".uplaod-file .browse-button input:file").change(function (){
    $("input[name='attachment']").each(function() {
      var fileName = $(this).val().split('/').pop().split('\\').pop();
      $(".filename").val(fileName);
      $(".browse-button-text").html('<i class="fa fa-refresh"></i> Change');
      $(".clear-button").show();
    });
  });
  
  //actions happening when the button is clicked
//   $('.clear-button').click(function(){
//       $('.filename').val("");
//       $('.clear-button').hide();
//       $('.browse-button input:file').val("");
//       $(".browse-button-text").html('<i class="fa fa-folder-open"></i> Browse'); 
//   });
  
  // --
  $(document).on('click', '.upload-field', function(){
    var file = $(this).parent().parent().parent().find('.input-file');
    file.trigger('click');
  });
  $(document).on('change', '.input-file', function(){
    $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
  });
  

//   Date time
if (/Mobi/.test(navigator.userAgent)) {
    $(".date-time input").attr("type", "datetime-local");
    $(".date input").attr("type", "date");
    $(".time input").attr("type", "time");
  } else {
    $("#datetimepicker").datetimepicker({
      icons: {
        time: "far fa-clock",
        date: "far fa-calendar-alt",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        next: "fa fa-chevron-right",
        previous: "fa fa-chevron-left"
      }
    });
    $("#datepicker").datetimepicker({
      format: "L",
      icons: {
        next: "fa fa-chevron-right",
        previous: "fa fa-chevron-left"
      }
    });
    $("#timepicker").datetimepicker({
      format: "LT",
      icons: {
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down"
      }
    });
  }