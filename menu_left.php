<div class="col-md-3">                        
    <div class="aside">
        <div class="aside-mobile-header">
            <h2 class="title">
                FILTER
                <i class="fas fa-filter"></i>
            </h2>
        </div>
        <div class="aside-mobile-body">
            <div class="aside-categories">
                <h2 class="title">CATEGORIES</h2>
                <ul>
                    <li class="active"><a href="product.php">BEDROOM<label>(256)</label></a></li>
                    <li><a href="product.php">LIVING ROOM<label>(177)</label></a></li>
                    <li><a href="product.php">KITCHEN<label>(236)</label></a></li>
                    <li><a href="product.php">DINING ROOM<label>(450)</label></a></li>
                    <li><a href="product.php">HOME AND LIVING<label>(123)</label></a></li>
                    <li><a href="product.php">HOME DECOR<label>(325)</label></a></li>
                    <li><a href="product.php">OUTDOOR<label>(81)</label></a></li>
                </ul>
            </div>
            <div class="row-fuild">
                <div class="aside-categories">
                    <h2 class="title">MATERIAL</h2>
                    <label class="control control--checkbox">TEAK
                        <input type="checkbox" checked="checked"/>
                        <div class="control__indicator"></div>
                    </label>
                    <label class="control control--checkbox">PADAUK
                        <input type="checkbox"/>
                        <div class="control__indicator"></div>
                    </label>
                    <label class="control control--checkbox">PAYOONG
                        <input type="checkbox" />
                        <div class="control__indicator"></div>
                    </label>
                    <label class="control control--checkbox">DAENG
                        <input type="checkbox"/>
                        <div class="control__indicator"></div>
                    </label>
                    <label class="control control--checkbox">MAKHA WOOD
                        <input type="checkbox"/>
                        <div class="control__indicator"></div>
                    </label>
                    <label class="control control--checkbox">OTHER WOOD
                        <input type="checkbox"/>
                        <div class="control__indicator"></div>
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>