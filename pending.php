<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li> 
                    <li><a title="ACCOUNT ORDER" href="account_order.php">ACCOUNT ORDER</a></li>         
                    <li><a title="PENDING" class="active" href="pending.php">PENDING</a></li>        
                </ol>
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">                
                <h1 class="heading-title row-fluid">PENDING</h1>
            </div>
        </section>

        <section class="row-fluid ">
            <div class="container">

                <div class="row-fluid form-group">
                    <div class="row-fluid empty-space" style="margin-top:50px;"></div>
                    <div class="pull-left">
                        <span class="plain-text bold">#12345900AFT</span><br>
                        <span class="plain-text">ORDER DATE : 06/06/2561</span><br>
                        <span class="plain-text">STATUS : PENDING</span>
                    </div>
                    <a href="invoice-print.php" target="_blank" class="btn btn-print pull-right">
                        <i class="fa fa-print"></i>
                        PRINT ORDER
                    </a>
                    <div class="row-fluid empty-space" style="margin-top:50px;"></div>
                </div>

                <div class="row-fluid cart-wrapper cart-verify">
                    <div class="row-fluid cart-header">
                        <ul>
                            <li>PRODUCT</li>
                            <li>DETAIL</li>
                            <li>PRICE</li>
                            <li>QTY</li>
                            <li>SUBTOTLE</li>
                        </ul>
                    </div>
                    <div class="row-fluid cart-body">
                        <?php for ($i=1; $i<=3 ; $i++) {?>
                            <div class="cart-body-inner">
                                <div class="cart-img">
                                    <a class="" href="productdetail.php" title="Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an">
                                        <img alt="" class="" src="images/product-<?php echo "$i"; ?>.png">
                                    </a>
                                </div>
                                <div class="cart-desc">
                                    <a class="" href="productdetail.php" title="Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an">
                                        <span class="text text-black" >
                                            Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, nihil expetendis in mei. Mei an 
                                        </span>
                                        <span class="ID text-gray">UK89456</span>
                                    </a>
                                </div>
                                <div class="cart-price">
                                    <span>2,500</span>
                                </div>
                                <div class="cart-qty">
                                    <span class="total-amount">2</sapn>
                                </div>
                                <div class="cart-subtotal">
                                    <span>2,500</span>
                                </div>      
                            </div>
                        <?php } ?>
                    </div> 
                    
                    <div class="row-fluid form-group">
                        <div class="row">                           
                            <div class="col-md-4 full-width-xs">
                                <h4 class="row-fluid form-group bold">SHIPPING ADDRESS</h4>
                                <div class="address-panel">
                                    <span class="row-fluid text plain-text">
                                        TOSAPOL CHUENMALA<br>
                                        120/34-35 MOO 24<br>
                                        SILA SUB DISTRICT, <br>
                                        MUEANG KHONKAEN DISTRICT, <br>
                                        KHONKAEN PROVINCE. <br>
                                        40000,<br>
                                         THAILAND
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4 full-width-xs">    
                                <div class="row-fluid">
                                    <h4 class="row-fluid form-group bold">BILLING ADDRESS 1</h4>
                                    <div class="address-panel">
                                        <span class="row-fluid text plain-text">
                                            TOSAPOL CHUENMALA<br>
                                            120/34-35 MOO 24<br>
                                            SILA SUB DISTRICT, <br>
                                            MUEANG KHONKAEN DISTRICT, <br>
                                            KHONKAEN PROVINCE.<br>
                                            40000,<br>
                                            THAILAND
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 full-width-xs">
                                <h4 class="row-fluid form-group bold">TOTAL</h4>
                                <div class="address-panel">
                                    <span class="row-fluid text plain-text">
                                        <p class="pull-left">SUBTOTAL </p>
                                        <p class="pull-right">25,000 .-</p>
                                    </span>
                                    <span class="row-fluid text plain-text">
                                        <p class="pull-left"> DELIVERY TIME</p>
                                        <p class="pull-right">5 DAYS</p>
                                    </span>
                                    <span class="row-fluid text plain-text">
                                        <p class="pull-left"> SHIPPING FEE </p>
                                        <p class="pull-right">200 .-</p>
                                    </span>
                                    <span class="row-fluid text plain-text">
                                        <p class="pull-left bold"> TOTAL (VAT) </p>
                                        <p class="pull-right">25,200 .-</p>
                                    </span>
                                </div>
                                <h4 class="row-fluid form-group bold">PAYMENT METHOD</h4>                                
                                <div class="row-fluid ">
                                    <div class="address-panel form-group">
                                        <span class="text text-black">BANK TRANSFER</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



<!-- upload file -->
                    <div class="row-fluid">
                        <h4 class="row-fluid form-group bold">PAYMENT INFORMATION METHOD</h4>
                        <div class="address-panel">
                            <form class="address-form">
                                <div class="row">
                                    <div class="col-md-4">
                                        <input type="text" name="" class="input-control form-group" readonly="" value="12345900AFT" placeholder="*ORDER NUMBER" >
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="" class="input-control form-group" placeholder="*FIRST NAME">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="" class="input-control form-group" placeholder="*LAST NAME">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="email" name="" class="input-control form-group" placeholder="*EMAIL">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="" class="input-control form-group" placeholder="*TELEPHONE">
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <span class="row-fluid form-group plain-text bold">MAKE A PAYMENT INTO</span>
                                    <div class="row-fluid">
                                        <div class="row">
                                            <div class="col-md-6 full-width-xs">
                                                <div class="bank-method-img bank-method-select">
                                                    <ul>
                                                        <li class="bank-list active">
                                                            <i class="material-icons radio-icons">lens</i>
                                                            <img src="images/k-bank.jpg">
                                                            <span class="text">BIG HOME FURNISH 302-15-13-789</span>
                                                        </li>
                                                        <li class="bank-list">
                                                            <i class="material-icons radio-icons">lens</i>
                                                            <img src="images/scb-bank.png">
                                                            <span class="text">BIG HOME FURNISH 302-15-13-789</span>
                                                        </li>
                                                        <li class="bank-list">
                                                            <i class="material-icons radio-icons">lens</i>
                                                            <img src="images/tmb-bank.png">
                                                            <span class="text">BIG HOME FURNISH 302-15-13-789</span>
                                                        </li>
                                                        <li class="bank-list">
                                                            <i class="material-icons radio-icons">lens</i>
                                                            <img src="images/gsb-bsnk.jpg">
                                                            <span class="text">BIG HOME FURNISH 302-15-13-789</span>
                                                        </li>
                                                        <li class="bank-list">
                                                            <i class="material-icons radio-icons">lens</i>
                                                            <img src="images/ktbthai-bank.jpg">
                                                            <span class="text">BIG HOME FURNISH 302-15-13-789</span>
                                                        </li>
                                                        <li class="bank-list">
                                                            <i class="material-icons radio-icons">lens</i>
                                                            <img src="images/tnc-bank.jpg">
                                                            <span class="text">BIG HOME FURNISH 302-15-13-789</span>
                                                        </li>
                                                        <li class="bank-list">
                                                            <i class="material-icons radio-icons">lens</i>
                                                            <img src="images/krungsri-bank.jpg">
                                                            <span class="text">BIG HOME FURNISH 302-15-13-789</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-6 full-width-xs">
                                                <input type="text" name="" class="input-control form-group" placeholder="*AMOUNT">
                                                <div class="row-fluid">
                                                    <div class="input-group date-time" id="datetimepicker">
                                                        <input class="input-control " placeholder="*PAYMENT DATE AND TIME" value=""/>
                                                        <span class="input-group-addon">
                                                            <span class="far fa-calendar-alt"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <input type="file" name="" class="input-file">
                                                    <div class="upload-file input-group">                                                        
                                                        <input type="text" class="form-control input-control" disabled placeholder="*UPLOAD BANK SLIP">
                                                        <span class="input-group-btn">
                                                            <button class="upload-field btn btn-success" type="button">
                                                            <i class="fa fa-folder-open"></i> 
                                                            Browse</button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <input type="text" name="" class="input-control form-group" placeholder="*REMARK">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-send-email">REPORT</button>
                            </form>
                        </div>
                    </div> 
<!-- end of upload file --> 



                </div> 
            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">
                <h2 class="heading-title row-fluid">PENDING</h2>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>

    </div>    

    <?php include "footer.php"; ?>

</body>
</html>