<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <?php include "cat_menu.php"; ?>

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li>            
                    <li><a title="PRODUCT" class="active" href="product.php">PRODUCT</a></li>        
                </ol>
            </div>
        </section>

        <?php include "search_box.php"; ?>

        <section class="row-fluid">  
            <div class="container"> 
                <div class="row">

                    <?php include "menu_left.php"; ?>

                    <div class="col-md-9 full-width-xs">
                        <div class="content-wrapper">
                            <div class="box-heading row-fluid">
                                <h5 class="title pull-left">ALL PRODUCT</h5>
                                <div class="right-group">
                                    <div class="filter-group">
                                        <div class="">
                                            <label>SORT BY PRICE</label>
                                            <select class="select">
                                                <option>0-1,000</option>
                                                <option>1,001-2,000</option>
                                                <option>2,001-3,000</option>
                                                <option>3,001-4,000</option>
                                                <option>4,001 ขึ้นไป</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="pagination">
                                        <ul>
                                            <li><a><i class="material-icons">first_page</i></a></li>
                                            <li class="active"><a>1</a></li>
                                            <li><a>2</a></li>
                                            <li><a>3</a></li>
                                            <li>...</li>
                                            <li><a>60</a></li>
                                            <li><a><i class="material-icons">last_page</i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body row-fluid">
                                <div class="row">
                                    <?php for ($i=0; $i<=2 ; $i++) {?>
                                        <div class="col-sm-6 col-lg-4">
                                            <div class="item-fill">
                                                <div class="item-fill-inner"> 
                                                    <div class="image">
                                                        <a title="NATURAL WOOD TABLE" href="productdetail.php">
                                                            <img alt="" class="" src="images/product-1.png?v=1" />
                                                        </a>
                                                    </div>
                                                    <h3 class="titel">NATURAL WOOD TABLE</h3>
                                                    <h5 class="description">
                                                        ແຕ່ບໍ່ໄດ້ຮັບການປະຕິບັດໃນໄລຍະຜ່ານມາ, ຂ້າພະເຈົ້າໄດ້ເພີ່ມຂຶ້ນໃນ. 
                                                        ຂ້ອຍເປັນຄົນທໍາອິດທີ່ຂ້ອຍຮູ້ວ່າຂ້ອຍມີຄວາມຮູ້ສຶກວ່າຂ້ອຍມີຄວາມຮູ້ສຶກແນວໃດ? 
                                                        ສາມາດເຂົ້າເຖິງຂໍ້ມູນທີ່ມີຢູ່, ຫຼືກ່ຽວຂ້ອງກັບຄວາມຫມາຍ, ຄວາມຜິດພາດ
                                                    </h5>
                                                    <span class="sell-price price">
                                                        360.00 .-
                                                    </span>
                                                    <button type="submit" class="btn btn-add-to-cart" data-toggle="modal" data-target="#exampleModal">ADD TO CART</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-lg-4">
                                            <div class="item-fill">
                                                <div class="item-fill-inner"> 
                                                    <div class="image">
                                                        <a title="NATURAL WOOD TABLE" href="productdetail.php">
                                                            <img alt="" class="" src="images/product-2.png?v=1" />
                                                        </a>
                                                    </div>
                                                    <h6 class="promotion-label">SPECIAL PROMOTION</h6>                                        
                                                    <h3 class="titel">NATURAL WOOD TABLE</h3>
                                                    <h5 class="description">
                                                        Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, 
                                                        nihil expetendis in mei. Mei an pericula euripidis, hinc partem
                                                        ei est. Eos ei nisl graecis, vix aperiri consequat an. Eius lorem 
                                                        tincidunt vix at, vel pertinax sensibus id, error epicurei.
                                                    </h5>
                                                    <span class="discount-price price">
                                                        360.00 .-
                                                    </span>
                                                    <span class="original-price price">
                                                        450.00 .-
                                                    </span>
                                                    <button type="submit" class="btn btn-add-to-cart" data-toggle="modal" data-target="#exampleModal">ADD TO CART</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-lg-4">
                                            <div class="item-fill">
                                            <div class="item-fill-inner"> 
                                                    <div class="image">
                                                        <a title="NATURAL WOOD TABLE" href="productdetail.php">
                                                            <img alt="" class="" src="images/product-3.png?v=1" />
                                                        </a>
                                                    </div>
                                                    <h6 class="promotion-label">SPECIAL PROMOTION</h6>    
                                                    <h3 class="titel">NATURAL WOOD TABLE</h3>
                                                    <h5 class="description">
                                                        Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, 
                                                        nihil expetendis in mei. Mei an pericula euripidis, hinc partem
                                                        ei est. Eos ei nisl graecis, vix aperiri consequat an. Eius lorem 
                                                        tincidunt vix at, vel pertinax sensibus id, error epicurei.
                                                    </h5>
                                                    <span class="discount-price price">
                                                        360.00 .-
                                                    </span>
                                                    <span class="original-price price">
                                                        450.00 .-
                                                    </span>
                                                    <button type="submit" class="btn btn-add-to-cart" data-toggle="modal" data-target="#exampleModal">ADD TO CART</button>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>                    
                            </div> 
                            <div class="box-heading row-fluid">
                                <h5 class="title pull-left">ALL PRODUCT</h5>
                                <div class="right-group">
                                    <div class="filter-group">
                                        <div class="">
                                            <label>SORT BY PRICE</label>
                                            <select class="select">
                                                <option>0-1,000</option>
                                                <option>1,001-2,000</option>
                                                <option>2,001-3,000</option>
                                                <option>3,001-4,000</option>
                                                <option>4,001 ขึ้นไป</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="pagination">
                                        <ul>
                                            <!-- <span>PAGE :</span> -->
                                            <li><a><i class="material-icons">first_page</i></a></li>
                                            <li class="active"><a>1</a></li>
                                            <li><a>2</a></li>
                                            <li><a>3</a></li>
                                            <li>...</li>
                                            <li><a>60</a></li>
                                            <li><a><i class="material-icons">last_page</i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>  
                        </div>   
                    </div>
                </div>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>


    </div>    

    <?php include "footer.php"; ?>

</body>
</html>