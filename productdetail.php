<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section> 

        <?php include "cat_menu.php"; ?>

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li>    
                    <li><a title="PRODUCT" href="product.php">PRODUCT</a></li>         
                    <li><a title="PRODUCT DETAIL" class="active" href="productdetail.php">PRODUCT DETAIL</a></li>        
                </ol>
            </div>
        </section>

        <?php include "search_box.php"; ?>

        <section class="row-fluid"> 
            <div class="container">                
                <h1 class="heading-title row-fluid">PRODUCT DETAIL</h1>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:30px;"></div>
        <section class="row-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 full-width-xs form-group">
                        <div class="row-fluid">
                            <div class="slider slider-for ">
                                <div class="slider-thumb">
                                    <img src="http://bridge82.qodeinteractive.com/wp-content/uploads/2016/08/animals.jpg">
                                </div>
                                <div class="slider-thumb">
                                    <img src="http://bridge221.qodeinteractive.com/wp-content/uploads/2018/04/slider-1.jpg">
                                </div>
                                <div class="slider-thumb">
                                    <img src="images/product-3.png?v=1">
                                </div>
                                <div class="slider-thumb">
                                    <img src="images/product-1.png?v=1">
                                </div>
                                <div class="slider-thumb">
                                    <img src="images/product-2.png?v=1">
                                </div>
                            </div>
                            <div class="slider slider-nav">
                                <div class="slider-thumb">
                                    <img src="http://bridge82.qodeinteractive.com/wp-content/uploads/2016/08/animals.jpg">
                                </div>
                                <div class="slider-thumb">
                                    <img src="http://bridge221.qodeinteractive.com/wp-content/uploads/2018/04/slider-1.jpg">
                                </div>
                                <div class="slider-thumb">
                                    <img src="images/product-3.png?v=1">
                                </div>
                                <div class="slider-thumb">
                                    <img src="images/product-1.png?v=1">
                                </div>
                                <div class="slider-thumb">
                                    <img src="images/product-2.png?v=1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 full-width-xs form-group">
                        <div class="row-fluid product-details">
                            <span class="plain-text text-orange">SPECIAL PROMOTION</span>
                            <br>
                            <h3 class="title">NATURAL WOOD TABLE</h3>
                            <div class="sub-title">
                                <span class="strong">PRODUCT CODE :</span>   
                                <span>UK89456</span>                                    
                            </div>
                            <br><br>
                            <div class="sub-title">
                                <span class="strong">KEY FEATURES :</span>   
                                <span>SOLID WOOD CONSTRUCTION IN FRANCH ROMANTIC STYLE</span>                                    
                            </div>
                            <div class="sub-title">
                                <span class="strong">MATERIAL :</span>   
                                <span>TREAK, PADAUK</span>                                    
                            </div>
                            <div class="sub-title">
                                <span class="strong">SIZE W*D*H :</span>   
                                <span>90*57.5*60 CM.</span>                                    
                            </div>
                            <div class="sub-title">
                                <span class="strong">COLOR :</span>   
                                <span>BROWN</span>                                    
                            </div>
                            <div class="product-thumbnail">
                                <ul class="select-color">
                                    <li class="color color-red active">
                                        <img alt="" class="" src="images/product-1.png?v=1" />
                                        <i class="material-icons">signal_cellular_4_bar</i>
                                    </li>
                                    <li class="color color-orange">
                                        <img alt="" class="" src="images/product-1.png?v=1" />
                                        <i class="material-icons">signal_cellular_4_bar</i>
                                    </li>
                                    <li class="color color-green">
                                        <img alt="" class="" src="images/product-1.png?v=1" />
                                        <i class="material-icons">signal_cellular_4_bar</i>
                                    </li>
                                <ul>                                                          
                            </div>
                            <div class="row-fluid form-group">
                                <div class="cart-qty">
                                    <div class="qty-group">
                                        <div class="FormUnit FormUnit--spin js_spin">
                                            <input class="FormUnit-field FormUnit-field--spin js_spin-input" type="number" min="0" max="20" step="1" value="1" name="quantity" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid form-group">
                                <span class="discount-price row-fluid price">
                                    360.00 .-
                                </span>
                                <span class="original-price row-fluid price">
                                    450.00 .-
                                </span>
                            </div>
                            <button type="submit" class="btn btn-add-to-cart" data-toggle="modal" data-target="#exampleModal">ADD TO CART</button>                      

                        </div> 
                    </div>
                </div>  

            </div>
        </section>

        <section class="row-fluid"> 
            <div class="container">
                <h2 class="heading-title row-fluid">RELATED PRODUCTS</h2>
            </div>
        </section>
        <div class="row-fluid empty-space" style="margin-top:15px;"></div>

        <section class="row-fluid panel-padding bg-softgray bg-img">
            <div class="container">
                <div class="box-body row-fluid">
                    <div class="row related-slick">                    
                        <div class="col-sm-6 col-lg-3">
                            <div class="item-fill">
                                <div class="item-fill-inner"> 
                                    <div class="image">
                                        <a title="NATURAL WOOD TABLE" href="productdetail.php">
                                            <img alt="" class="" src="images/product-1.png?v=1" />
                                        </a>
                                    </div>
                                    <a class="text-black link" title="NATURAL WOOD TABLE" href="productdetail.php">
                                        <h3 class="titel">NATURAL WOOD TABLE</h3>
                                        <h5 class="description">
                                            ແຕ່ບໍ່ໄດ້ຮັບການປະຕິບັດໃນໄລຍະຜ່ານມາ, ຂ້າພະເຈົ້າໄດ້ເພີ່ມຂຶ້ນໃນ. 
                                            ຂ້ອຍເປັນຄົນທໍາອິດທີ່ຂ້ອຍຮູ້ວ່າຂ້ອຍມີຄວາມຮູ້ສຶກວ່າຂ້ອຍມີຄວາມຮູ້ສຶກແນວໃດ? 
                                            ສາມາດເຂົ້າເຖິງຂໍ້ມູນທີ່ມີຢູ່, ຫຼືກ່ຽວຂ້ອງກັບຄວາມຫມາຍ, ຄວາມຜິດພາດ
                                        </h5>
                                        <span class="sell-price price">
                                            360.00 .-
                                        </span>
                                    </a>                                                    
                                    <button type="submit" class="btn btn-add-to-cart" data-toggle="modal" data-target="#exampleModal">ADD TO CART</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="item-fill">
                                <div class="item-fill-inner"> 
                                    <div class="image">
                                        <a title="NATURAL WOOD TABLE" href="productdetail.php">
                                            <img alt="" class="" src="images/product-2.png?v=1" />
                                        </a>
                                    </div>
                                    <a class="text-black link" title="NATURAL WOOD TABLE" href="productdetail.php">
                                        <h6 class="promotion-label">SPECIAL PROMOTION</h6>                                        
                                        <h3 class="titel">NATURAL WOOD TABLE</h3>
                                        <h5 class="description">
                                            Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, 
                                            nihil expetendis in mei. Mei an pericula euripidis, hinc partem
                                            ei est. Eos ei nisl graecis, vix aperiri consequat an. Eius lorem 
                                            tincidunt vix at, vel pertinax sensibus id, error epicurei.
                                        </h5>
                                        <span class="discount-price price">
                                            360.00 .-
                                        </span>
                                        <span class="original-price price">
                                            450.00 .-
                                        </span>
                                    </a>
                                    <button type="submit" class="btn btn-add-to-cart" data-toggle="modal" data-target="#exampleModal">ADD TO CART</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="item-fill">
                            <div class="item-fill-inner"> 
                                    <div class="image">
                                        <a title="NATURAL WOOD TABLE" href="productdetail.php">
                                            <img alt="" class="" src="images/product-3.png?v=1" />
                                        </a>
                                    </div>
                                    <a class="text-black link" title="NATURAL WOOD TABLE" href="productdetail.php">
                                        <h3 class="titel">NATURAL WOOD TABLE</h3>
                                        <h5 class="description">
                                            Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, 
                                            nihil expetendis in mei. Mei an pericula euripidis, hinc partem
                                            ei est. Eos ei nisl graecis, vix aperiri consequat an. Eius lorem 
                                            tincidunt vix at, vel pertinax sensibus id, error epicurei.
                                        </h5>
                                        <span class="sell-price price">
                                            360.00 .-
                                        </span>
                                    </a>
                                    <button type="submit" class="btn btn-add-to-cart" data-toggle="modal" data-target="#exampleModal">ADD TO CART</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="item-fill">
                            <div class="item-fill-inner"> 
                                    <div class="image">
                                        <a title="NATURAL WOOD TABLE" href="productdetail.php">
                                            <img alt="" class="" src="images/product-3.png?v=1" />
                                        </a>
                                    </div>
                                    <a class="text-black link" title="NATURAL WOOD TABLE" href="productdetail.php">
                                        <h3 class="titel">NATURAL WOOD TABLE</h3>
                                        <h5 class="description">
                                            Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, 
                                            nihil expetendis in mei. Mei an pericula euripidis, hinc partem
                                            ei est. Eos ei nisl graecis, vix aperiri consequat an. Eius lorem 
                                            tincidunt vix at, vel pertinax sensibus id, error epicurei.
                                        </h5>
                                        <span class="sell-price price">
                                            360.00 .-
                                        </span>
                                    </a>
                                    <button type="submit" class="btn btn-add-to-cart" data-toggle="modal" data-target="#exampleModal">ADD TO CART</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="item-fill">
                                <div class="item-fill-inner"> 
                                    <div class="image">
                                        <a title="NATURAL WOOD TABLE" href="productdetail.php">
                                            <img alt="" class="" src="images/product-1.png?v=1" />
                                        </a>
                                    </div>
                                    <a class="text-black link" title="NATURAL WOOD TABLE" href="productdetail.php">
                                        <h3 class="titel">NATURAL WOOD TABLE</h3>
                                        <h5 class="description">
                                            ແຕ່ບໍ່ໄດ້ຮັບການປະຕິບັດໃນໄລຍະຜ່ານມາ, ຂ້າພະເຈົ້າໄດ້ເພີ່ມຂຶ້ນໃນ. 
                                            ຂ້ອຍເປັນຄົນທໍາອິດທີ່ຂ້ອຍຮູ້ວ່າຂ້ອຍມີຄວາມຮູ້ສຶກວ່າຂ້ອຍມີຄວາມຮູ້ສຶກແນວໃດ? 
                                            ສາມາດເຂົ້າເຖິງຂໍ້ມູນທີ່ມີຢູ່, ຫຼືກ່ຽວຂ້ອງກັບຄວາມຫມາຍ, ຄວາມຜິດພາດ
                                        </h5>
                                        <span class="sell-price price">
                                            360.00 .-
                                        </span>
                                    </a>                                                    
                                    <button type="submit" class="btn btn-add-to-cart" data-toggle="modal" data-target="#exampleModal">ADD TO CART</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="item-fill">
                                <div class="item-fill-inner"> 
                                    <div class="image">
                                        <a title="NATURAL WOOD TABLE" href="productdetail.php">
                                            <img alt="" class="" src="images/product-1.png?v=1" />
                                        </a>
                                    </div>
                                    <a class="text-black link" title="NATURAL WOOD TABLE" href="productdetail.php">
                                        <h3 class="titel">NATURAL WOOD TABLE</h3>
                                        <h5 class="description">
                                            ແຕ່ບໍ່ໄດ້ຮັບການປະຕິບັດໃນໄລຍະຜ່ານມາ, ຂ້າພະເຈົ້າໄດ້ເພີ່ມຂຶ້ນໃນ. 
                                            ຂ້ອຍເປັນຄົນທໍາອິດທີ່ຂ້ອຍຮູ້ວ່າຂ້ອຍມີຄວາມຮູ້ສຶກວ່າຂ້ອຍມີຄວາມຮູ້ສຶກແນວໃດ? 
                                            ສາມາດເຂົ້າເຖິງຂໍ້ມູນທີ່ມີຢູ່, ຫຼືກ່ຽວຂ້ອງກັບຄວາມຫມາຍ, ຄວາມຜິດພາດ
                                        </h5>
                                        <span class="sell-price price">
                                            360.00 .-
                                        </span>
                                    </a>                                                    
                                    <button type="submit" class="btn btn-add-to-cart" data-toggle="modal" data-target="#exampleModal">ADD TO CART</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="item-fill">
                                <div class="item-fill-inner"> 
                                    <div class="image">
                                        <a title="NATURAL WOOD TABLE" href="productdetail.php">
                                            <img alt="" class="" src="images/product-1.png?v=1" />
                                        </a>
                                    </div>
                                    <a class="text-black link" title="NATURAL WOOD TABLE" href="productdetail.php">
                                        <h3 class="titel">NATURAL WOOD TABLE</h3>
                                        <h5 class="description">
                                            ແຕ່ບໍ່ໄດ້ຮັບການປະຕິບັດໃນໄລຍະຜ່ານມາ, ຂ້າພະເຈົ້າໄດ້ເພີ່ມຂຶ້ນໃນ. 
                                            ຂ້ອຍເປັນຄົນທໍາອິດທີ່ຂ້ອຍຮູ້ວ່າຂ້ອຍມີຄວາມຮູ້ສຶກວ່າຂ້ອຍມີຄວາມຮູ້ສຶກແນວໃດ? 
                                            ສາມາດເຂົ້າເຖິງຂໍ້ມູນທີ່ມີຢູ່, ຫຼືກ່ຽວຂ້ອງກັບຄວາມຫມາຍ, ຄວາມຜິດພາດ
                                        </h5>
                                        <span class="sell-price price">
                                            360.00 .-
                                        </span>
                                    </a>                                                    
                                    <button type="submit" class="btn btn-add-to-cart" data-toggle="modal" data-target="#exampleModal">ADD TO CART</button>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </section>

<!-- Dont hav slick -->
         <section class="row-fluid panel-padding bg-softgray bg-img">
            <div class="container">
                <div class="box-body row-fluid">
                    <div class="row ">                    
                        <div class="col-sm-6 col-lg-3">
                            <div class="item-fill">
                                <div class="item-fill-inner"> 
                                    <div class="image">
                                        <a title="NATURAL WOOD TABLE" href="productdetail.php">
                                            <img alt="" class="" src="images/product-1.png?v=1" />
                                        </a>
                                    </div>
                                    <a class="text-black link" title="NATURAL WOOD TABLE" href="productdetail.php">
                                        <h3 class="titel">NATURAL WOOD TABLE</h3>
                                        <h5 class="description">
                                            ແຕ່ບໍ່ໄດ້ຮັບການປະຕິບັດໃນໄລຍະຜ່ານມາ, ຂ້າພະເຈົ້າໄດ້ເພີ່ມຂຶ້ນໃນ. 
                                            ຂ້ອຍເປັນຄົນທໍາອິດທີ່ຂ້ອຍຮູ້ວ່າຂ້ອຍມີຄວາມຮູ້ສຶກວ່າຂ້ອຍມີຄວາມຮູ້ສຶກແນວໃດ? 
                                            ສາມາດເຂົ້າເຖິງຂໍ້ມູນທີ່ມີຢູ່, ຫຼືກ່ຽວຂ້ອງກັບຄວາມຫມາຍ, ຄວາມຜິດພາດ
                                        </h5>
                                        <span class="sell-price price">
                                            360.00 .-
                                        </span>
                                    </a>                                                    
                                    <button type="submit" class="btn btn-add-to-cart" data-toggle="modal" data-target="#exampleModal">ADD TO CART</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="item-fill">
                                <div class="item-fill-inner"> 
                                    <div class="image">
                                        <a title="NATURAL WOOD TABLE" href="productdetail.php">
                                            <img alt="" class="" src="images/product-2.png?v=1" />
                                        </a>
                                    </div>
                                    <a class="text-black link" title="NATURAL WOOD TABLE" href="productdetail.php">
                                        <h6 class="promotion-label">SPECIAL PROMOTION</h6>                                        
                                        <h3 class="titel">NATURAL WOOD TABLE</h3>
                                        <h5 class="description">
                                            Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, 
                                            nihil expetendis in mei. Mei an pericula euripidis, hinc partem
                                            ei est. Eos ei nisl graecis, vix aperiri consequat an. Eius lorem 
                                            tincidunt vix at, vel pertinax sensibus id, error epicurei.
                                        </h5>
                                        <span class="discount-price price">
                                            360.00 .-
                                        </span>
                                        <span class="original-price price">
                                            450.00 .-
                                        </span>
                                    </a>
                                    <button type="submit" class="btn btn-add-to-cart" data-toggle="modal" data-target="#exampleModal">ADD TO CART</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="item-fill">
                            <div class="item-fill-inner"> 
                                    <div class="image">
                                        <a title="NATURAL WOOD TABLE" href="productdetail.php">
                                            <img alt="" class="" src="images/product-3.png?v=1" />
                                        </a>
                                    </div>
                                    <a class="text-black link" title="NATURAL WOOD TABLE" href="productdetail.php">
                                        <h3 class="titel">NATURAL WOOD TABLE</h3>
                                        <h5 class="description">
                                            Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, 
                                            nihil expetendis in mei. Mei an pericula euripidis, hinc partem
                                            ei est. Eos ei nisl graecis, vix aperiri consequat an. Eius lorem 
                                            tincidunt vix at, vel pertinax sensibus id, error epicurei.
                                        </h5>
                                        <span class="sell-price price">
                                            360.00 .-
                                        </span>
                                    </a>
                                    <button type="submit" class="btn btn-add-to-cart" data-toggle="modal" data-target="#exampleModal">ADD TO CART</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="item-fill">
                            <div class="item-fill-inner"> 
                                    <div class="image">
                                        <a title="NATURAL WOOD TABLE" href="productdetail.php">
                                            <img alt="" class="" src="images/product-3.png?v=1" />
                                        </a>
                                    </div>
                                    <a class="text-black link" title="NATURAL WOOD TABLE" href="productdetail.php">
                                        <h3 class="titel">NATURAL WOOD TABLE</h3>
                                        <h5 class="description">
                                            Alienum phaedrum torquatos nec eu, vis detraxit periculis ex, 
                                            nihil expetendis in mei. Mei an pericula euripidis, hinc partem
                                            ei est. Eos ei nisl graecis, vix aperiri consequat an. Eius lorem 
                                            tincidunt vix at, vel pertinax sensibus id, error epicurei.
                                        </h5>
                                        <span class="sell-price price">
                                            360.00 .-
                                        </span>
                                    </a>
                                    <button type="submit" class="btn btn-add-to-cart" data-toggle="modal" data-target="#exampleModal">ADD TO CART</button>
                                </div>
                            </div>
                        </div>                        
                    </div>                    
                </div>
            </div>
        </section>
        
        <div class="row-fluid empty-space" style="margin-top:15px;"></div>
        <section class="row-fluid"> 
            <div class="container">
                <h2 class="heading-title row-fluid">PRODUCT DETAIL</h2>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>

    </div>    

    <?php include "footer.php"; ?>

     <script>
        // QTY
        (function (win, doc) {
            'use strict';
            if (!doc.querySelector || !win.addEventListener || !doc.documentElement.classList) {
                return;
            }

            var Spinner = function(rootElement) {
                //Add button selector
                var addButtonSelector = '.js_spin-add';
                //Remove button selector
                var removeButtonSelector = '.js_spin-remove';
                //Number input selector
                var numberInputSelector = '.js_spin-input';
                //A variable to store the markup for the add button
                var addButtonMarkup = '<button type="button" class="FormUnit-quantity FormUnit-quantity--add js_spin-add Icon Icon--isClosed"><i class="material-icons">add</i></button>';
                //A variable to store the markup for the remove button
                var removeButtonMarkup = '<button type="button" class="FormUnit-quantity FormUnit-quantity--remove js_spin-remove Icon Icon--remove"><i class="material-icons">remove</i></button>';
                //Variable to store the root's container
                var container;
                //A variable for the markup of the number input pattern
                var markup;
                //A variable to store a number input
                var numberInput;
                //Variable to store the add button
                var addButton;
                //Variable to store the remove button
                var removeButton;
                //Store max value
                var maxValue;
                //Store min value
                var minValue;
                //Store step value
                var step;
                //Store new value
                var newValue;
                //Variable to store the loop counter
                var i;

                //Initialisation function
                this.init = function() {
                    container = rootElement;
                    //Get the markup inside the number input container
                    markup = container.innerHTML;
                    //Create a button to decrese the value by 1
                    markup += removeButtonMarkup;
                    //Create a button to increase the value by 1
                    markup += addButtonMarkup;
                    //Update the container with the new markup
                    container.innerHTML = markup;

                    //Get the add and remove buttons
                    addButton = rootElement.querySelector(addButtonSelector);
                    removeButton = rootElement.querySelector(removeButtonSelector);
                    
                    //Get the number input element
                    numberInput = rootElement.querySelector(numberInputSelector);

                    //Get min, max and step values
                    if (numberInput.hasAttribute('max')) {
                        maxValue = parseInt(numberInput.getAttribute('max'), 10);
                    } else {
                        maxValue = 99999;
                    }
                    if (numberInput.hasAttribute('min')) {
                        minValue = parseInt(numberInput.getAttribute('min'), 10);
                    } else {
                        minValue = 0;
                    }
                    if (numberInput.hasAttribute('step')) {
                        step = parseInt(numberInput.getAttribute('step'), 10);
                    } else {
                        step = 1;
                    }

                    //Change the number input type to text
                    numberInput.setAttribute('type', 'text');
                    
                    //If there is there no pattern attribute, set it to only accept numbers
                    if (!numberInput.hasAttribute('pattern')) {
                        numberInput.setAttribute('pattern', '[0-9]');
                    }

                    //Add click events to the add and remove buttons
                    addButton.addEventListener('click', add, false);  
                    removeButton.addEventListener('click', remove, false);
                };

                //Methods for setting values
                this.setAddButtonMarkup = function(markup) {
                    addButtonMarkup = markup;
                };

                this.setRemoveButtonMarkup = function(markup) {
                    removeButtonMarkup = markup;
                };

                this.setAddButtonSelector = function(selector) {
                    addButtonSelector = selector;
                };

                this.setRemoveSelector = function(selector) {
                    removeButtonSelector = selector;
                };

                this.setNumberInputSelector = function(selector) {
                    numberInputSelector = selector;
                };

                //Function to add one to the quantity value
                var add = function(ev) {
                    newValue = parseInt(numberInput.value, 10) + step;
                    //If the value is less than the max value
                    if (newValue <= maxValue) {
                        //Add one to the number input value
                        numberInput.value = newValue;
                        //Button is active
                        removeButton.disabled = false;
                    }
                    //If the value is equal to the max value
                    if (numberInput.value == maxValue || newValue > maxValue) {
                        //Disable the button
                        addButton.disabled = true;
                    }
                    ev.preventDefault();
                };
                //Function to subtract one from the quantity value
                var remove = function(ev) {
                    newValue = parseInt(numberInput.value, 10) - step;
                    //If the number input value is bigger than the min value, reduce the the value by 1
                    if (newValue >= minValue) {
                        numberInput.value = newValue;
                        addButton.disabled = false;
                    }
                    //If the input value is the min value, add disabled property to the button
                    if (numberInput.value == minValue || newValue < minValue) {
                        removeButton.disabled = true;
                    }
                    ev.preventDefault();
                };
            };

            //Get all of the number input elements
            var spins = doc.querySelectorAll('.js_spin');
            //Store the total number of number inputs
            var spinsTotal = spins.length;
            //A variable to store one number inputs
            var spin;
            //A counter for the loop
            var i;
            //Loop through each number input
            for ( i = 0; i < spinsTotal; i = i + 1 ) {
                //Create a new Spin object for each number input
                spin = new Spinner(spins[i]);
                //Start the initialisation function
                spin.init();
            }

        }(this, this.document));

    </script>

</body>
</html>