<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <?php include "cat_menu.php"; ?>

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li>   
                    <li><a title="RESULT" href="searchnotfound.php">RESULT</a></li>            
                </ol>   
            </div>
        </section>

        <?php include "search_box.php"; ?>

        <section class="row-fluid">  
            <div class="container"> 
                <div class="row">
                    
                    <?php include "menu_left.php"; ?>

                    <div class="col-md-9 full-width-xs">
                        <div class="content-wrapper">
                            <div class="box-heading row-fluid">
                                <h5 class="title pull-left">SEARCH NOTFOUND</h5>                                
                            </div>
                            <div class="box-body row-fluid">
                                <center>
                                    <h3>NOTFOUND<h3>
                                    <h4>Sorry for not Can not find what you're looking for.<h4>
                                    <i class="material-icons notfound-icons">search</i>
                                </center>                 
                            </div>                             
                        </div>   
                    </div>
                </div>
            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>


    </div>    

    <?php include "footer.php"; ?>

</body>
</html>