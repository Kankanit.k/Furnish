<!DOCTYPE html>
<html>
<?php include "head.php"; ?>
<body>
    <?php include "header.php"; ?>

    <div class="main-weapper">

        <section class="main-banner-wrapper">
            <div class="main-banner">
                <div class="image">
                    <img class="full-width" src="images/bg.jpg?v=1">
                </div>
            </div>
        </section>  

        <section class="row-fluid"> 
            <div class="container">
                <ol class="row-fluid breadcrumb">
                    <li><a title="HOME" href="index.php">HOME</a></li>          
                    <li><a title="SIGN UP" class="active" href="signup.php">SIGN UP</a></li>        
                </ol>
            </div>
        </section>

        <section class="row-fluid">  
            <div class="container">                
                <div class="main-content row-fluid">                    
                    <h1 class="heading-title row-fluid">SIGN UP</h1>
                    
                    <div class="row-fluid content-inner">
                        <form role="form" method="" class="form-sign-up">
                            <div class="row">
                                <div class="col-md-6 full-width-xs">
                                    <input type="text" name="" class="input-control form-group" placeholder="*FIRST NAME" />
                                    <input type="text" name="" class="input-control form-group" placeholder="*LAST NAME" />
                                    <input type="email" name="" class="input-control form-group" placeholder="*EMAIL" />
                                    <input type="password" name="" class="input-control form-group" placeholder="*PASSWORD" />
                                    <input type="password" name="" class="input-control form-group" placeholder="*CONFIRM PASSWORD" />
                                </div>
                                <div class="col-md-6 full-width-xs">
                                    <button type="submit" class="btn btn-send-email sign-up">SIGN ME UP</button>
                                </div>
                            </div>      
                        </form>                
                    </div>
                    <h2 class="heading-title row-fluid">SIGN UP</h2>
                </div>

            </div>
        </section>

        <div class="row-fluid empty-space" style="margin-top:50px;"></div>
        <section class="row-fluid">
            <div class="image">
                <img class="full-width" src="images/bg-bottom.jpg?v=1">
            </div>
        </section>

    </div>    

    <?php include "footer.php"; ?>

</body>
</html>